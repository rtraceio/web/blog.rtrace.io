FROM fedora:latest

EXPOSE 80

RUN dnf up -y && dnf install nginx -y && mkdir -p /var/run/nginx-cache
COPY /public/ /var/www/blog
COPY /config/nginx.conf /etc/nginx/nginx.conf
COPY /config/blog.nginx.conf /etc/nginx/conf.d/blog.nginx.conf

CMD ["nginx", "-g", "daemon off;"]