---
title: Impressum
authors: 
    - raffael@rtrace.io
tags:
    - Impressum
    - Terms
---

Impressum, Offenlegungspflicht, verantwortlicher Herausgeber, Webdesign & Entwicklung, DMCA, Terms of Service

<!-- more -->

Im Sinne der Offenlegungspflicht / In terms of the disclosure obligation:

- Raffael Rehberger
- Private Website / Private Website
- Artikel zu Technologie und Musikproduktionen / Articles on technology and music productions
- 4614 Marchtrenk, Oberösterreich, Österreich / 4614 Marchtrenk, Upper Austria, Austria

## Für diese Website verantwortlicher Herausgeber

- **Name:** Raffael Rehberger
- **E-Mail:** [webmaster@rtrace.io](mailto:webmaster@rtrace.io)
- **Internet:** [blog.rtrace.io](https://blog.rtrace.io)

Für den Inhalt externer Seiten, auf die verlinkt wird, liegt die alleinige Verantwortung beim jeweiligen Anbieter.
The sole responsibility for the content of external pages to which links are provided lies with the respective provider.

## Webdesign & Entwicklung / Web design & development

- **Name:** Raffael Rehberger
- **E-Mail:** [webmaster@rtrace.io](mailto:webmaster@rtrace.io)
- **Internet:** [blog.rtrace.io](https://blog.rtrace.io)

### DMCA Inquiries

in case of DMCA inquiries, please contact: [dmca@rtrace.io](mailto:dmca@rtrace.io).
DMCA takedown requests are processed within 5 workdays.

### Terms of Service

- `rtrace.io` reserves the right to change this policy at any time without prior notification.
- Do not upload or (most importantly) distribute copyrighted or illegal material.
- When you are distributing copyrighted material always ask for permission from the owners first.
- All content whether public or private, is the sole responsibility of the person who created/uploaded it.
- Services provided on `rtrace.io` are provided "as is" without warranty of any kind.
- in general: just be a nice person
