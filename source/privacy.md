---
title: Privacy Policy
authors: 
    - raffael@rtrace.io
tags:
    - Privacy
    - Privacy Policy
    - Terms
---

If any user data is collected, the data is ONLY used to provide the services.
No personal data is ever shared with a third-party. No personal data is recorded and processed.

<!-- more -->

## In General

If any user data is collected, the data is ONLY used to provide the services.
No personal data is ever shared with a third-party. No personal data is recorded and processed.

## Cookies

In general, there are no tracking-cookies in place. Neither do I want to be tracked, nor do I want to track you.

- `blog.rtrace.io` (Blog, Hexo) does NOT utilize cookies
- `live.rtrace.io` (Owncast) does NOT utilize cookies
- `speedtest.rtrace.io` (Librespeed) does NOT utilize cookies
- `reddit.rtrace.io` (Libreddit) does NOT utilize cookies, except (optional) user-configuration
- `lws.rtrace.io` (LiberaWebStore) does NOT utilize cookies
- `static.rtrace.io` (Static File Server) does NOT utilize cookies
- `searx.rtrace.io` (Searx metasearch enging) does NOT utilize cookies, except (optional) user-configuration
- `send.rtrace.io` (Send, Filesharing Service) does NOT utilize cookies
- `bibliogram.rtrace.io` (privacy friendly Instagram frontend) does NOT utilize cookies
- `vikunja.rtrace.io` (a F(L)OSS task manager/organizer) does utilize Cookies for Session Management
- `status.rtrace.io` (Status Monitor) does NOT utilize cookies

## Logs

Each site-request (targeting `*.rtrace.io` and `rtrace.io`) is logged. The following information will be stored for a max. of 4 weeks, then will be deleted permanently.

- **Time of request**: a simple UNIX timestamp representing the time of request
- **User-Agent**: taken from the Requests HTTP-Header
- **IP-Address**: the remote IP address of the client that is connecting
- **Requested Site**: the site on `*.rtrace.io` or `rtrace.io` that has been requested

These logs exclusively serve to protect from attacks (like DoS) against servers and infrastructure of `rtrace.io`. None of this information will be processed nor used for any analytic purposes. Logs will be retained for a max. period of 4 weeks on the servers serving `*.rtrace.io` and `rtrace.io`. No third-party does and will ever have access to these logs.

## Site Analytics

The following services provided on this domain utilize [plausible.io](https://plausible.io/) as a privacy-friendly site-analytics framework. Plausible does not track any GDPR-related information. For further details, please also read [what plausible tracks](https://plausible.io/privacy-focused-web-analytics). For further protection of your identity, requests to plausible are proxied through `rtrace.io`.

## Site Embeds

Some blog posts, articles and pages on `blog.rtrace.io` embed so called `iframes` to other services and websites. Where possible, I use embeds of privacy-friendly services.

### Invidious

[Invidious](https://redirect.invidious.io/) is a privacy-friendly frontend for [YouTube](https://youtube.com). To prevent you from being tracked by Google when visiting `*.rtrace.io` and `rtrace.io`, all video-embeds are served through randomly selected Invidious instances.

### Owncast

[Owncast](https://owncast.online/) is a self-hosted free and open source live-streaming solution with integrated chat. Owncast is known to be a privacy-friendly alternative to Twitch. `rtrace.io` hosts Owncast on [live.rtrace.io](https://live.rtrace.io).

### Soundcloud

[Soundcloud](https://soundcloud.com) is an audio-streaming service (just like Spotify). `blog.rtrace.io` uses Soundcloud embeds on certain pages. Embeds will store cookies. Please follow [Soundclouds Privacy Policy](https://soundcloud.com/pages/privacy) for more details. (Please note, that I'm working on replacing Soundcloud with [Funkwhale](https://funkwhale.audio/)).

### Mixcloud

[Mixcloud](https://mixcloud.com) is an audio-streaming service (just like Spotify). `blog.rtrace.io` uses Mixcloud embeds on certain pages. Embeds will store cookies. Please follow [Mixcloud Privacy Policy](https://mixcloud.com/privacy/) for more details. (Please note, that I'm working on replacing Mixcloud with [Funkwhale](https://funkwhale.audio/))

### Asciinema

[Asciinema](https://asciinema.org/) offers an alternative solution to record and share terminal sessions. `blog.rtrace.io` uses Asciinema embeds on certain pages. Embeds will store cookies. Please folllow [Asciinema Privacy Policy](https://asciinema.org/privacy) for more details.

## Further transparency

### Hosting

All services provided on `rtrace.io` and `*.rtrace.io` are hosted on Virtual Private Servers (VPS) at [Netcup](https://netcup.de) (subsidiary company of [Anexia GmbH](https://anexia.com/)). Servers are located in Nuremberg, 🇩🇪 Germany. Servers are running the latest version of Fedora Server and are patched ~2 times a week. Disks are LUKS encrypted and require LUKS password on boot. All services provided are served through `nginx` acting as reverse-proxy/load-balancer. `nginx` also terminates TLS (HTTPS). Upstream-Servers behind the reverse-proxy are interconnected through Wireguard for secure communication - so none of your traffic (even if previously TLS-terminated) will ever be transferred over the network in plain-text. TLS Certificates are provided by [Let's Encrypt](https://letsencrypt.org/). TLS configuration explicitly only allows `TLSv1.3` with the strongest available cipher-suites. [HTTP Security Headers](https://securityheaders.com/?q=blog.rtrace.io&followRedirects=on) are applied where possible and useful.

<a href="https://www.netcup.de" target="_blank">
    <img src="/images/hosters/netcup-logo.png" width="440" height="200" alt="netcup.de" />
</a>

### Security

If you are a security researcher interested in testing/analyzing service on `rtrace.io` or would like to report a security vulnerability, please refer to the [Security Policy](/security).

### Personal Commitment

I personally hate it to be tracked online, and that's why I won't track you either (and try my best to prevent third parties from spying on you too). It's that easy and that's my promise. I want all services provided at `rtrace.io` and all of its subdomains to be as privacy-friendly as possible. If you have any additions, constructive feedback, hints to improve or wishes - feel free to [contact](/about/my/pgp) me at any time. As it is with security - at the end of the day everything boils down to `trust`. It's very hard to trust service-providers and that effectively boils down to the fact that you can't and shouldn't trust me - zero-trust is the best trust after all).

Nonetheless, if you want to verify that the things I've written here are true, please get in touch with me. We can arrange a Jitsi call, and I'll happily show you how things are hosted and configured here at `rtrace.io` - come on, let's go for a SSH safari.
