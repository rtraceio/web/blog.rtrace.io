---
title: "[🎶Release]: Sixpence None The Richer - There She Goes (Ruffys LoFi Edit)"
date: 2025-01-31
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - Bitwig
    - LoFi
    - HipHop
    - Cover
    - Ruffy Le RaRe

categories:
    - Music Production

photos:
    - /images/albumart/albumart-there-she-goes.jpg

toc: true
comments: true
---

It's time for another LoFi gem. Following up on `Wouldn’t It Be Nice`, this release takes on `Sixpence None The Richer`'s version of `The La`’s classic "There She Goes". If I had to sum it up in three words: smooth, dreamy, and effortlessly chill. This track combines soft ukulele, warm rhythm guitar, and beautiful female vocals, resulting in an atmospheric and relaxing song that instantly sets the vibe. Whether you’re unwinding, studying, or just getting lost in the moment, it’s the perfect tune to sink into. Hit play, sit back, and let this one carry you away. 🌙✨

<!-- more -->

## Introduction

It's time for another LoFi gem. Following up on `Wouldn’t It Be Nice`, this release takes on `Sixpence None The Richer`'s version of `The La`’s classic "There She Goes". If I had to sum it up in three words: smooth, dreamy, and effortlessly chill. This track combines soft ukulele, warm rhythm guitar, and beautiful female vocals, resulting in an atmospheric and relaxing song that instantly sets the vibe. Whether you’re unwinding, studying, or just getting lost in the moment, it’s the perfect tune to sink into. H

![CoverArt](/images/albumart/albumart-there-she-goes.jpg)


## Downloads

| Track                                   | Type    | Download-Link                                                       | Password    |
|:---------------------------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| There She Goes (Ruffys LoFi Radio Edit) | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/V2CBJC0ASM#2w1GT8alTC4F) | `rtrace.io` |


## Streams

{% soundcloud track 2022921144 %}
                     

{% youtube I7NQF8GFeGk %}


### Streaming sites

- ![Soundcloud](/img/soundcloud-16x16.png) [Soundcloud](https://soundcloud.com/djraremusic/sixpence-none-the-richer-there-she-goes-ruffys-lofi-edit)
- ![Funkwhale](/img/funkwhale-16x16.png) [Funkwhale](https://am.pirateradio.social/library/tracks/80216/)
- ![YouTube](/img/youtube-16x16.png) [YouTube](https://www.youtube.com/watch?v=I7NQF8GFeGk)
- ![Bandcamp](/img/bandcamp-16x16.png) [Bandcamp](https://2phaze.bandcamp.com/track/sixpence-none-the-richer-there-she-goes-ruffys-lofi-edit)



## Lyrics

```text
There she goes
There she goes again
Racing through my brain
And I just can't contain
This feeling that remains

There she goes
There she goes again
Pulsing through my veins
And I just can't contain
This feeling that remains

There she goes (There she goes again)
There she goes again (There she goes again)
Racing through my brain (There she goes again)
And I just can't contain
This feeling that remains

There she goes
There she goes again
She calls my name
Pulls my train
No one else could heal my pain
And I just can't contain
There she goes (She calls my name)
There she goes again (She calls my name)
Chasing down my lane (She calls my name)
And I just can't contain
This feeling that remains

There she goes (There she goes again)
There she goes (There she goes again)
There she goes
``` 

## Have Fun

Downloads of the tracks are FREE. I really hope you enjoy this song! If you want to give me feedback on `There She Goes` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io).

## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)