---
title: '[🎶Release]: Holding You'
date: 2022-09-17
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - FL Studio
    - Hands Up
    - 2Phaze
    - Holding You

categories:
    - Music Production

photos:
    - /images/albumart/albumart-2phaze-holding-you.jpg

toc: true
comments: true
---

Here we go again! `2Phaze` is back with a new original tune called "Holding You". `Holding You` is a romantic dance duet in the typical 2Phaze Hands Up style. "Hands Up Will Never Die" - with this in mind, ruffy produced "Holding You" as a catchy dance tune with lovely vocals and lyrics telling you the story of a couple in love, and how they describe their relationship.

<!-- more -->

## Introduction

Sometimes I do forget how much effort it is, to finish up a production from beginning to end. Working on `Holding You` was one of the most fascinating experiences so far, but also one of the most challenging productions. With around 400 hours of continuous working- and production-time, it took ~9 months until it was finally finished and entered the state of release-worthiness.

Holding You is released under my producer-alias `2Phaze`, and it is a romantic dance duet in the typical 2Phaze Hands Up style. With "Hands Up Will Never Die", I tried to create yet another catchy dance tune with some lovely vocals.

## Downloads

| Track                      | Type    | Download-Link                                                       | Password    |
|:--------------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| Holding You (Radio Edit)   | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/CRVT307ZYW#UWsl3E8Vdt2B) | `rtrace.io` |
| Holding You (Extended Mix) | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/TJSBM66MPC#YOyYbzJbNS8b) | `rtrace.io` |


## Streams

## Melli Mellow's Video Edit

Huge shout out to `mel`, for creating yet another Lyrics Video Cut! Thanks `mel`!

{% youtube 2eejCF8OMSo %}

### Extended Club Mix

If you are a DJ, then this is the Extended Version you're looking for!
Holding You plays at exactly 157 BPM.

{% soundcloud track 1345663801 %}

### Radio Edit

If you just want to casually listen to "Holding You" without waiting too long to get to the beautiful vocals! This is the edit you're looking for!
{% soundcloud track 1345664914 %}

### Streaming sites

- ![iTunes](/img/itunes-16x16.png) [Apple Music](https://music.apple.com/us/album/holding-you-single/1646496649)
- ![Spotify](/img/spotify-16x16.png) [Spotify](https://open.spotify.com/album/6WuqaJU8F6cvhZJKT5MMCS)
- ![Deezer](/img/deezer-16x16.png) [Deezer](https://www.deezer.com/de/album/357869997)
- ![Amazon Music](/img/amazon-music-16x16.png) [Amazon Music](https://music.amazon.com/albums/B0BFP3F7CZ)
- ![Soundcloud](/img/soundcloud-16x16.png) [Soundcloud](https://soundcloud.com/djraremusic/2phaze-holding-you-radio-edit)

## Collabs

I had the pleasure to collaborate with Chelsea from 🇿🇦 South Africa once again. But this time her husband Richard joined in as duet partner. Chelsea and Rich made the vocals come alive, they made "Holding You" to what it was meant to be. No doubt, they are probably the most talented singers and vocalists out there. 

Oleksandara, a guitarist originally from 🇺🇦 Ukraine, helped me with the electric-guitar backtrack used throughout the verses and as sidechain element in the 4-to-the-floor parts of "Holding You".

## Lyrics

The vocal arrangement, lead vocals and lyrics come from [Stan Walker](https://en.wikipedia.org/wiki/Stan_Walker) and [Ginny Blackmore](https://en.wikipedia.org/wiki/Ginny_Blackmore). Those 2 artists are from 🇳🇿 New Zealand and collaborated on the original "Holding You". Together they created this heart-warming masterpiece. Usually I'm not so much into such hearty tracks, but "Holding You" had me from the first moment on.

{% invidious obUro4TrJmw %}

Stan Walker himself, perfectly described the lyrics as: "a real hearty story about love and how grateful and lucky we are to have that person in our life". I couldn't agree more to what Stan said.

```text
[Verse]
It's no surprise that we ain't in heaven,
We're on the Earth.
But you, you meet me in my darkest cavern,
Can't count what you are worth.

[Bridge]
'Cause, baby when the waves crash over me,
Baby, when the skies turn crazy
You're the only one I want next to me, me.

[Chorus]
With every passing second, every drop of falling rain
I realize I'm lucky to be holding you
When my ground starts shaking
And my bones start breaking
I realize I'm lucky to be holding you
We ain't in a perfect world
But you still bring the stars out
You still freak my heart out,
Oh, oh, oh
With every passing second, every drop of falling rain
I realize I'm lucky, I'm lucky, I'm lucky to be holding you

[Melody/Build-Up/Bridge]
...

[DROP]
Ha! (Vocal Shout)

We ain't in a perfect world
But you still bring the stars out
You still freak my heart out,
With every passing second, every drop of falling rain
I realize I'm lucky, I'm lucky, I'm lucky to be holding you

[Vocal Cuts]
(no lyrics here lol ^^)

[Verse]
Thought I would always be alone
Then you, you walked in like a dream
Your smile shone brighter than the sun to me,
Your heart is next to none

[Bridge]
'Cause, baby when the waves crash over me,
Baby, when the skies turn crazy
You're the only one I want next to me, me.

[Chorus]
With every passing second, every drop of falling rain
I realize I'm lucky to be holding you
When my ground starts shaking
And my bones start breaking
I realize I'm lucky to be holding you
We ain't in a perfect world
But you still bring the stars out
You still freak my heart out,
Oh, oh, oh
With every passing second, every drop of falling rain
I realize I'm lucky, I'm lucky, I'm lucky to be holding you

[Bridge]
And I know, I know
What we've got is far greater than a dream
You've become my everything
I know, I know
We ain't perfect but I want you everyday
Could I be that lucky?

[Outro]
With every passing second, every drop of falling rain
I realize I'm lucky to be holding you
When my ground starts shaking
And my bones start breaking
I realize I'm lucky to be holding you
We ain't in a perfect world
But you still bring the stars out
You still freak my heart out,
Oh, oh, oh
With every passing second, every drop of falling rain
I realize I'm lucky, I'm lucky, I'm lucky to be holding you
``` 

## Have Fun

Downloads of the tracks are FREE. I really hope you enjoy this song! If you want to give me feedback on `Holding You` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io).

## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)