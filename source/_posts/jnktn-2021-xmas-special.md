---
title: 🎅 X-MAS Special 2021 @ Jnktn.TV
date: 2021-12-08
authors: 
    - raffael@rtrace.io
tags:
    - Streaming
    - Audio
    - Video
    - Music
    - Owncast
    - Jnktn.TV
    - DJ

categories:
    - Streaming
    - Music

photos:
    - /images/jnktn/jnktn-xmas-logo.png
    - /images/jnktn/jnktn-logo.png

toc: false
comments: true
---

We're getting closer and closer to Christmas. So close, that one can already smell cookies 🍪 and the scent of the Christmas tree 🎄 in the living room. It's the time where we're supposed to calm down and just enjoy the time with our family and loved ones. Yet here we are stressing ourselves out, often for absolutely no reason at all. But, as always, [Jntktn.TV](https://jnktn.tv) got your back. The crew prepared a super awesome X-MAS Special just for you, so you can lean back and just enjoy the show. The lovely streamers of `Jnktn` will be live for you on Saturday the *`18th of December`* for a total of 13 (!!!) hours. Come for the music, stay for the awesome people, and forget about the "business as usual" for a moment.

<!-- more -->

## Introduction

We're getting closer and closer to Christmas. So close, that one can already smell cookies 🍪 and the scent of the Christmas tree 🎄 in the living room. It's the time where we're supposed to calm down and just enjoy the time with our family and loved ones. Yet here we are stressing ourselves out, often for absolutely no reason at all. But, as always, [Jntktn.TV](https://jnktn.tv) got your back. The crew prepared a super awesome X-MAS Special just for you, so you can lean back and just enjoy the show. The lovely streamers of `Jnktn` will be live for you on Saturday the *`18th of December`* for a total of 13 (!!!) hours. Come for the music, stay for the awesome people, and forget about the "business as usual" for a moment.

![Jnktn.TV](/images/jnktn/jnktn-xmas-logo.png)

## Hard Facts

- *When?*: Saturday, `18th of December`, starting at 12:00 (UTC / UK time)
- *Where?*: [Jnktn.TV](https://jnktn.tv)
- *What to prepare?*: Dancing shoes, mulled wine, beer, coffee, tea, cookies and snacks

## What's on menu?

The team prepared a `13 hours` schedule for Saturday the 18th December. Until now, that's the longest continuous stream in the projects history. That alone is already a great reason to celebrate. But that's not the only reason, Jnktn is also happy to see a new face on board. [@hejo](https://hejo.org) - a already well-known Owncaster (in German I'd say: "ein alter Hase") - will have his premiere on Jnktn that day as well. Hejo, I wish you all the best here at Jnktn and great fun with your kick-off show.

Of course, the usual suspects at Jnktn have a timeslot as well that day.

- *12:00* - *13:30* - [jumboshrimp's](https://fosstodon.org/@jumboshrimp) Catch of the Week
- *13:30* - *14:30* - [JC's](https://jnktn.tv) Juice Box
- *14:30* - *16:00* - Dance Attack with [Ruffy](https://mastodon.social/@rarepublic)
- *16:00* - *17:30* - [Vera's](https://jnktn.tv) Radio Show
- *17:30* - *18:30* - Unpopular Stream with [Michan](https://fosstodon.org/@meisam)
- *18:30* - *20:00* - [Hejo's](https://hejo.org) Tunes
- *20:00* - *21:30* - Get ready with [Andrina](https://jnktn.tv)
- *21:30* - *23:00* - [Gary's](https://jnktn.tv) Citrus Club
- *23:00* - *01:00* - [Worky](https://www.instagram.com/worky_gla/)

The schedule says it all. We're in for a whole day of awesomeness, don't miss it.

## Where?

Well it's easy. Just open your browser and navigate to [Jnktn.TV](https://jnktn.tv) and press the play button. You'll find the Schedule on the bottom of the page. You might want to create an account on the chat-platform to reserve your nickname. Feel free to chat with the lovely people in the chat - they won't bite. Promised!

### Jnktn.TV on Social Media

if you have an account on any of these, it'd be great if you could leave a "Follow" there. Jnktn has earned it

- [Mastodon](https://mastodon.online/@jnktn_tv)
- [Instagram](https://www.instagram.com/jnktn.tv/)
- [Bibliogram (privacy friendly Instagram Frontend)](https://bibliogram.rtrace.io/u/jnktn.tv)
- [Mixcloud](https://www.mixcloud.com/jnktn_tv/)

### Others

- [Jnktn.TV Sourcecode on Codeberg](https://codeberg.org/jnktn.tv)
- [TeeMill Merch Store](https://jnktn.teemill.com/)
- [Buy Jnktn.TV a coffee on Ko-Fi](https://ko-fi.com/jnktntv)
- [Donate via LiberaPay](https://liberapay.com/Jnktn.tv/donate)
- [Donate via OpenCollective](https://opencollective.com/jnktn-tv)

![Jnktn XMAS Schedule 2021](/images/jnktn/jnktn-xmas-schedule-2021.jpg)

## Related Posts

- [I am resident DJ at Jnktn.TV 🔥](/posts/jnktn-2021-ruffy-goes-jnktn/)
