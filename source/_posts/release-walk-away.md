---
title: '[🎶Release]: Walk Away (Ruffy Le RaRe Remix)'
date: 2022-02-20
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Video
    - Music
    - Release
    - Remix
    - FL Studio
    - FLP
    - House
    - Electronic
    - Ruffy Le RaRe
    - Asher Postman
    - Walk Away

categories:
    - Music Production

photos:
    - /images/albumart/albumart-asher-postman-walkaway.jpg
    - /images/artists/artist-asher-postman.jpg
    - /images/artists/artist-annelisa-franklin.jpg

toc: true
comments: true
---

It became silent around `Ruffy Le RaRe` for a few months now (again), but that does not mean there's no news at all. While university was taking most of my attention throughout the last few months, that didn't stop me from working on some of my older music projects on the side. And luckily I was able to finish one. Now with this post I'm proudly releasing my `Walk Away` Remix for [Asher Postman](https://soundcloud.com/asher-postman). This time it's a 115 BPM House'ish Remix, with a funky bassline and a Dubstep'ish finish (genre-bending is my favorite sport). Since many of you lovely people gave me ton of great feedback on the [last 2 releases with FLP](https://blog.rtrace.io/posts/release-unknown-paradigm-cold/) I also decided to publish the `FLP` (Fruity Loops Project File) again for this project!

<!-- more -->

## Introduction

It became silent around `Ruffy Le RaRe` for a few months now (again), but that does not mean there's no news at all. While university was taking most of my attention throughout the last few months, that didn't stop me from working on some of my older music projects on the side. And luckily I was able to finish one. Now with this post I'm proudly releasing my `Walk Away` Remix for [Asher Postman](https://soundcloud.com/asher-postman). This time it's a 115 BPM House'ish Remix, with a funky bassline and a Dubstep'ish finish (genre-bending is my favorite sport). Since many of you lovely people gave me ton of great feedback on the [last 2 releases with FLP](https://blog.rtrace.io/posts/release_unknownparadigm_cold/) I also decided to publish the `FLP` (Fruity Loops Project File) again for this project!

## Asher Postman

![Asher Postman in Studio](/images/artists/artist-asher-postman.jpg)

Asher Postman is a dance/electronic producer as well as a singer. Asher started making music in very young years from the basement of his parent's farmhouse in Lake Odessa, Michigan. He is famous for his meme videos and producing tutorials, as well as the original music he produces. His YouTube channel consists of a mix of remixes and production tutorial videos. The channel so far has over 180,000 subscribers and more than 29 million views. In other words, Asher Postman has attracted quite some popularity over time. He's also producing his music with FL Studio as `DAW`, which coincidentally is the same DAW I used for this Remix. If you're a producer as well, you might know Asher from his [Future House Demo Project](https://www.image-line.com/artists/asher-postman/) shipped with FL Studio. Also noteworthy, Asher released various tracks on [Armada Music](https://www.armadamusic.com/).

## Annelisa Franklin

![Annelisa Franklin](/images/artists/artist-annelisa-franklin.jpg)

Annelisa Franklin is a singer, vocalist, songwriter, recording artist and performer from Santa Monica, California. She has reinvented herself as a solo pop artist after a seven year run as lead singer in the sister duo, `Franklin Avenue`. Now she started a solo-carreer as vocalist/singer. Annelisa was not very well known in the Electronic Dance Music genres, but the collaboration with Asher Postman certainly changed that. The vocals of `Walk Away` were sung by Annelisa, and the outcome is astonishing.

## Walk Away (Ruffy Le RaRe Remix)

Here you'll find the free download links for the FLP and the High Quality FLAC audio file.

### Downloads

| Track                           | Type    | Download-Link                                                            | Password    |
|:-------------------------------:|:-------:|:------------------------------------------------------------------------:|:-----------:|
| Walk Away (Ruffy Le RaRe Remix) | HQ FLAC | [ProtonDrive](https://drive.protonmail.com/urls/K9WEJMTHRC#HNF3IJ4f2Zh3) | `rtrace.io` |
| Walk Away (Ruffy Le RaRe Remix) | FLP/ZIP | [ProtonDrive](https://drive.protonmail.com/urls/M3RTMG0GKM#8vyHyfwGRecO) | `rtrace.io` |

### Stream Online

{% youtube SI1MYzPSSp0 %}

{% soundcloud track 1221158164 %}

## Project

This remix is from a Remix Competition. All the credits for the vocals go to Asher. When I first heard about the Remix Contest I immediately fell in love with the vocals and knew I had to remix this beauty. It turned out as 115 BPM Hybrid between House/Dance and Dubstep with a slight touch of Funk. I think that's what people call genre-bending, isn't it?

What I personally really like about this track is the bassline in the first "drop". It was the reason why I purchased Sakura VST from Image-Line (just to bring this fascinating bassline to life). It's supposed to be a Fender Bass. While I'm somehow in doubt that this has anything to do with a Fender Bass, I really like it either way. The project is once again done with FL-Studio Stock plugins, so you can just download it and open it. Everything that was created outside of FL Studio was bounced and inserted as audio sample beforehand!

## Lyrics

```text
[Verse]
I don't wanna see you and all your friends
Wish that you would leave, so I can pretend that
I don't miss you baby, know that I've been lately
I don't wanna face it

[Pre-Chorus]
Oh, I know you know I want you all alone
But then I don't 'cause I got no control
Emotional, so you should let me go
Let me go

[Chorus]
If you could just walk away, yeah
I wouldn't even have to say it
'Cause right now I just need some space, yeah
So just walk away, just walk away

[Verse]
Staring at the floor
I don't wanna feel the way I did before
'Cause you make me feel like there's a hurricane up in my chest
I guess it's best I find shelter, then maybe I'll feel better, yeah

[Pre-Chorus]
Oh, I know you know I want you all alone
But then I don't 'cause I got no control
Emotional, so you should let me go
Let me go

[Chorus]
If you could just walk away, yeah
I wouldn't even have to say it
'Cause right now I just need some space, yeah
So just walk away, just walk away
I want, I wanna erase it
'Cause we could've been so amazing
But trust me if I could I'd change it
So please walk away, please walk away

[Bridge]
You make me honestly question myself
You make me act like I'm somebody else
You make me feel things that I've never felt
So just walk away, just walk away
You make me honestly question myself
You make me act like I'm somebody else
You make me feel things that I've never felt
So just walk away, just walk away
Walk away

[Chorus]
If you could just walk away, yeah
I wouldn't even have to say it
'Cause right now I just need some space, yeah
So just walk away, just walk away
I want, I wanna erase it
'Cause we could've been so amazing
But trust me if I could I'd change it
So please walk away, please walk away

[Outro]
So just walk away
So please walk away, please walk away
```

## Have Fun

Downloads of the track is FREE, also the FLPs are free to download for everyone interested. I really hope you enjoy this. If you want to remix/mashup/edit/bootleg the instrumental of `Walk Away`, please feel free to do so - just keep in mind to send me a mail with your results. If you include the original vocals, please make sure to contact Asher Postman and Armada Music beforehand! Would be happy to hear what you came up with. Also if you want to give me feedback (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io). Thanks.

## Links

- [Asher Postman on Soundcloud](https://soundcloud.com/asher-postman)
- [Asher Postman on YouTube](https://www.youtube.com/channel/UCiZs2DmLnyunAf9tQrZoXLg)
- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
