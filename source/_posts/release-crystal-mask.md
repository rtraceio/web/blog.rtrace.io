---
title: '[🎶Release]: Ruffy Le RaRe - Crystal Mask'
date: 2024-08-25
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - House
    - Electro
    - Complextro
    - Free FLP
    - Ruffy Le RaRe

categories:
    - Music Production

photos:
    - /images/albumart/album-art-ruffy-le-rare-crystal-mask.jpg

toc: true
comments: true
---

`Crystal Mask` is the new song by `Ruffy Le RaRe`. It's a dancefloor 4-to-the-floor Electro/Complextro tune, with a massive bassline and a powerful distorted saw lead. Simplistic in arrangement, but rich and deep in its sound design. Slightly atypical for `RLR` tracks, `Crystal Mask` plays at 132 BPM. Also most Ruffy Le RaRe tracks feature vocals, Crystal Mask is one of the rare exceptions until now that doesn't. Work on the song began almost 3 years ago. Now with around ~180 hrs of work invested into the project, `Crystal Mask` is finally released! Let's go! 🎶🎵🎵

<!-- more -->

## Introduction

`Crystal Mask` is the new song by `Ruffy Le RaRe`. It's a dancefloor 4-to-the-floor Electro/Complextro tune, with a massive bassline and a powerful distorted saw lead. Simplistic in arrangement, but rich and deep in its sound design. Slightly atypical for `RLR` tracks, `Crystal Mask` plays at 132 BPM. Also most Ruffy Le RaRe tracks feature vocals, Crystal Mask is one of the rare exceptions until now that doesn't. Work on the song began almost 3 years ago. Now with around ~180 hrs of work invested into the project, `Crystal Mask` is finally released! Let's go! 🎶🎵🎵

![Cover Art of Ruffy Le RaRe - Crystal Mask](/images/albumart/album-art-ruffy-le-rare-crystal-mask.jpg)

## Background 

`Crystal Mask` is a journey through different sounds, that while listening brings you back to the harder Electronic Dance Music in 2010. I started the project in August 2021, and 3 years later it's finally time to release it. Almost every synth you hear in `Crystal Mask` is synthesized with 3xOSC, once again showing how powerful it is, and that it doesn't need much to make something sound interesting. The trickiest part was the mixdown. Lots of elements in this dense mix fight for space, and finding a good balance was a massive effort. On the bright side, I learned so much in the progress. Shout out to Sinan from Futorial for some very helpful tips and tricks.

## Streaming

### Soundcloud

{% soundcloud track 1903785353 %}


### YouTube

{% youtube dAq8ulvfxQw %}

## Download

| Track               | Type    | Download-Link                                                       | Password    |
|:-------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| Club / Extended Mix | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/RHCZA368FG#IJIvnvIA0Ydw) | `rtrace.io` |
| FL Studio Project   | FLP     | [ProtonDrive](https://drive.proton.me/urls/PTY8QTF52W#Ill1SEUdaMfl) | `rtrace.io` |


## Have Fun

Download of the track is FREE as always. I really hope you enjoy this song! If you want to give me feedback on `Crystal Mask` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io) or a leave a comment below.


## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)