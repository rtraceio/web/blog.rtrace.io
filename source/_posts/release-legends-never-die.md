---
title: '[🎶Release]: Legends Never Die (2Phaze Hardstyle Remix)'
date: 2024-07-30
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - FL Studio
    - Hardstyle
    - Hands Up
    - Harddance
    - 2Phaze
    - Against The Current
    - League of Legends

categories:
    - Music Production

photos:
    - /images/albumart/albumart-legends-never-die.jpg
    - /images/artists/artists-against-the-current.jpg

toc: true
comments: true
---

If you're a `League of Legends` fan, you probably heard "Legends Never Die" by `Against The Current` already. But have you heard 2Phaze's Hardstyle Remix yet? If you're into high-energy and euphoric Hardstyle with an orchestral twist (some would call it Festival Hardstyle), this remix is a must-listen. 2Phaze has once again blended elements of Hardstyle, Hardcore, Hands Up, and Harddance into a single track. What is new for 2Phaze music is the orchestral and cinematic intro into the song. Let's listen, then let's play a round of LoL together 🎮

<!-- more -->

## Introduction

If you're a League of Legends fan, you probably heard "Legends Never Die" by `Against The Current` already. But have you heard 2Phaze's Hardstyle Remix yet? If you're into high-energy and euphoric Hardstyle with an orchestral twist (some would call it Festival Hardstyle), this remix is a must-listen. 2Phaze has once again blended elements of Hardstyle, Hardcore, Hands Up, and Harddance into a single track. What is new for 2Phaze music is the orchestral and cinematic intro into the song. Let's listen, then let's play a round of LoL together 🎮


![CoverArt](/images/albumart/albumart-legends-never-die-transparent.png)


## Against The Current

Against The Current is a pop-rock band coming from Poughkeepsie, New York. Formed in 2011, the group features lead vocalist Chrissy Costanza, guitarist Dan Gow, and drummer Will Ferri. Fans appreciate the band for their infectious melodies and well-written lyrics. Against The Current quickly rose to fame through their energetic live performances, but also thanks to their strong online presence. Their unique sound blends pop with rock, earning them a a massive international fanbase and multiple chart-topping hits.

![Band memebers of Against The Current](/images/artists/artists-against-the-current.jpg)


## Downloads

| Track                            | Type    | Download-Link                                                       | Password    |
|:--------------------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| Legends Never Die (2Phaze Remix) | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/TFHCHRK154#sNh3CABQ7mw8) | `rtrace.io` |


## Streams


### Soundcloud 

{% soundcloud track 1888360815 %}


### Invidious / YouTube

{% youtube -mWMcnfe-cg %}

### Streaming sites

- ![Soundcloud](/img/soundcloud-16x16.png) [Soundcloud](https://soundcloud.com/djraremusic/league-of-legends-legends-never-die-2phaze-remix)
- ![Funkwhale](/img/funkwhale-16x16.png) [Funkwhale](https://am.pirateradio.social/library/tracks/78574/)
- ![YouTube](/img/youtube-16x16.png) [YouTube](https://www.youtube.com/watch?v=AZ9mvK8aIz4)


## Lyrics

```
[Chorus]
Legends never die
When the world is callin' you
Can you hear them screaming out your name?
Legends never die
They become a part of you
Every time you bleed for reaching greatness
Relentless you survive

[Verse 1]
They never lose hope
When everything's cold and the fighting's near
It's deep in their bones
They'll run into smoke when the fire is fierce

[Pre-Chorus]
Oh, pick yourself up 'cause

[Chorus]
Legends never die
When the world is calling you
Can you hear them screamin' out your name?
Legends never die
They become a part of you
Every time you bleed for reaching greatness
Legends never die

[Post-Chorus]
They're written down in eternity
But you'll never see the price it costs
The scars collected all of their lives

[Verse 2]
When everything's lost
They pick up their hearts and avenge defeat
Before it all starts
They suffer through harm just to touch a dream

[Pre-Chorus]
Oh, pick yourself up 'cause

[Chorus]
Legends never die
When the world is calling you
Can you hear them screamin' out your name?
Legends never die
They become a part of you
Every time you bleed for reaching greatness
Legends never die

[Bridge]
When the world is callin' out your name
Begging you to fight

[Pre-Chorus]
Pick yourself up once more
Pick yourself up 'cause

[Chorus]
Legends never die
When the world is calling you
Can you hear them screamin' out your name?
Legends never die
They become a part of you
Every time you bleed for reaching greatness
Legends never die
```

## Have Fun

Downloads of the tracks are FREE. I really hope you enjoy this song! If you want to give me feedback on my `Legends Never Die Remix` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io) or comment anonymously below. Thanks.

## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)