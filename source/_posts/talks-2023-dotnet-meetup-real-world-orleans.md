---
title: Real-World Distributed Systems with Microsoft Orleans 
date: 2023-02-28
authors: 
    - raffael@rtrace.io
    - paul.rohorzka@squer.at
tags:
    - Microsoft Orleans
    - Actors
    - .NET
    - DotNetDevs Austria
    - Meetup
    - Distributed Systems
    - Architecture
    - Talk
    - Presentation
    - SQUER
    - Vienna

categories:
    - Development

photos:
    - /images/talks/dotnet-meetup-real-world-orleans.jpg

toc: false
comments: true
---

Today's architectural challenges are often tackled using distributed systems with microservices as major building blocks. While microservices offer great benefits, they tend to increase the overall complexity of a system. This problem especially increases in IoT scenarios with a large number of external systems to communicate with. In this talk, Raffael and Paul will show how actor frameworks such as Microsoft Orleans - using a long-forgotten paradigm from the early days of computer science - can greatly mitigate those problems. The speakers draw from their hands-on experience in developing a globally distributed system in .NET leveraging Microsoft Orleans.

<!-- more -->

## Hard facts

- 📆 When: 28th of February 2023, 1800 (UTC+1)
- 📍 Where: SQUER office, Marxergasse 17/1/33-34, 1030 Vienna, Austria 🇦🇹
- 📺 Live on [Twitch](https://www.twitch.tv/dotnetdevsat)
- 📺 Live on [YouTube](https://www.youtube.com/c/DotNetDevsAustria)
- 📺 Recoding on [Twitch](https://www.twitch.tv/videos/1751709423)
- 📺 Recoding on [YouTube](https://www.youtube.com/watch?v=-czMnCL2d1Y)
- 👨‍👩‍👧 Organized by: [.NET Devs Austria](https://dotnetdevs.at/)

## The talk

Today's architectural challenges are often tackled using distributed systems with microservices as major building blocks. While microservices offer great benefits, they tend to increase the overall complexity of a system. This problem especially increases in IoT scenarios with a large number of external systems to communicate with. In this talk, Raffael and Paul will show how actor frameworks such as Microsoft Orleans - using a long-forgotten paradigm from the early days of computer science - can greatly mitigate those problems. The speakers draw from their hands-on experience in developing a globally distributed system in .NET leveraging Microsoft Orleans.

{% youtube -czMnCL2d1Y %}

## Further Links

- [Paul on Twitter](https://nitter.kavin.rocks/paulroho)
- [SQUER](https://squer.at)
- [SQUER Meetup Group](https://www.meetup.com/de-DE/squer-brainfood-talks/)
- [Meetup Event](https://www.meetup.com/dotnet-austria/events/291441150)
- [.NET Devs Austria](https://dotnetdevs.at/)

![Overview Title Slide](/images/talks/dotnet-meetup-real-world-orleans.jpg)
