---
title: Installing Cordova with Android-support on Fedora
date: 2022-03-06
authors: 
    - raffael@rtrace.io
tags:
    - Coding
    - Development
    - Cordova
    - Android
    - Web
    - Fedora
    - Linux
    - Flatpak

categories:
    - Development

photos:
    - /images/software/cordova-logo.png

toc: true
comments: true
---

Apache Cordova is a cross-platform software-development framework allowing you to code your application once and have it running on Android, iOS, Web and Electron (Desktop) natively. Cordova itself might not be so well-known, yet it's the foundation for other Frameworks (such as Ionic) allowing cross-platform software development. While Cordova is a decent framework it can get quite hard to set it up correctly. This article will guide you through the installation of Cordova on Fedora (35 at the time of writing), so you can create your own apps with it.

<!-- more -->

## Introduction

Apache Cordova is a cross-platform software-development framework allowing you to code your application once and have it running on Android, iOS, Web and Electron (Desktop) natively. Cordova itself might not be so well-known, yet it's the foundation for other Frameworks (such as Ionic) allowing cross-platform software development. While Cordova is a decent framework it can get quite hard to set it up correctly. This article will guide you through the installation of Cordova on Fedora (35 at the time of writing), so you can create your own apps with it.

![Cordova Logo](/images/software/cordova-logo.png)

## Installation of Cordova

Before we get started with Cordova, we need to install a Node environment first. Cordova requires `node` and `npm` installed. Luckily Fedora ships recent versions of both in its repositories out-of-the-box.

```bash
sudo dnf install nodejs npm
```

Now that we have the `node` and `npm` out of the way, we can continue on installing cordova itself.

```bash
sudo npm install -g cordova
```

Let's create a dummy project verifying everything works as it should.

```bash
cordova create HelloWorld
cd HelloWorld/
cordova platform add browser
cordova build
cordova serve
```

That's it. Cordova is installed and everything works! Well, kind of. If you intend to create Web-Apps only, you can stop reading right here, and the installation is complete. If you're still interested in publishing to other Platforms such as Android, continue reading.

## Installing requirements for Android targets

### Install Android Studio

To summarize what's going to happen in this section, I'll just say: "We'll install the Android SDK and lots of other tooling necessary to get you going". Since I'm a huge fan of [Jetbrains](https://www.jetbrains.com) products myself, I'll show you the easy way of getting started with the Android SDK setup and let Android Studio do the heavy lifting. Luckily, `Android Studio` is available as Flatpak (make sure to have Flathub repositories referenced for the following steps to work).

```bash
flatpak install com.google.androidstudio
```

### Install Java

Android SDK tools are written in Java and require a Java environment to be set up and configured. Once again, Fedora got our back, and the only thing you'll have to do is to install the `openjdk` and then create an Environment-Variable called `JAVA_HOME` pointing to it.

```bash
sudo dnf install java-latest-openjdk java-latest-openjdk-devel

# this assignment will always use the path to the latest version of Java
# and thus it's even working when you update the openjdk via dnf
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")

# Verify Java installation was successful
javac --version
```

### Install Gradle

Additionally to Java, we'll need to have `gradle` installed. Gradle is a build automation framework for Java. It's extensively used in Cordova and Android development. Here we just grab a copy of the latest binaries from [The Gradle Release page](https://gradle.org/releases/). At the time of writing this article, the version was `7.4`. Make sure to adapt the version in the Download-URL accordingly (as argument to the `wget` command below).

```bash
GRADLE_VERSION="7.4"
wget "https://services.gradle.org/distributions/gradle-$GRADLE_VERSION-bin.zip"
unzip gradle-$GRADLE_VERSION-bin.zip
cp -r gradle-$GRADLE_VERSION-bin/ /usr/local/gradle
export PATH="$PATH:/usr/local/gradle/bin"

# Verify Gradle installation is succcessful
gradle --version
```

### Export Environment Variables

Once that is done, we'll need to export further environment variables for the Android SDK, so Cordova can find and use it.

```bash
export ANDROID_SDK_ROOT="/home/$(whoami)/Android/Sdk/"

# ANDROID_HOME is legacy, you might not need to set this.
# It's just here for backwards-compatibility purposes.
export ANDROID_HOME=$ANDROID_SDK_ROOT

export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/build-tools/32.0.0/"
```

### Install Android Build-Tools

Now we want to install the `Android Build Tools` in the right version, so Cordova can properly use them. As of writing this article, Cordova required `30.0.3` of the Android-build-tools. You can install the build tools through the `sdkmanager` coming with the Android SDK. To verify which version Cordova requires at the moment, you just use the Cordova CLI Command `cordova requirements` within a project. It'll tell you which version it expects.

```bash
sdkmanager --install "build-tools;30.0.3"
```

### Automatically set Environment Variables on Login

As you've seen you'll need a lot of environment variables to be set. Doing these manually every time you log into a shell is not convenient at all. Depending on the shell you are using, you can add these export statements at the end of its rc files. Those files are sourced everytime you instantiate a new shell.

```bash
# for ZSH edit the .zshrc file in your home directory
cat ~/.zshrc

# for Bash edit the .bashrc file in your home directory
cat ~/.bashrc
```

add the following environment variable export statements at the end of the corresponding rc file.

```bash
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")
export ANDROID_SDK_ROOT="/home/$(whoami)/Android/Sdk/"
export ANDROID_HOME=$ANDROID_SDK_ROOT
export PATH="$PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/tools/bin/"
export PATH="$PATH:$ANDROID_SDK_ROOT/platform-tools/"
export PATH="$PATH:$ANDROID_SDK_ROOT/build-tools/32.0.0/"
export PATH="$PATH:/usr/local/gradle/bin"
```

### Add Android Platform to Cordova project

now we can finalize the installation of Android-related tooling for Cordova. We just add the `android` platform to our previously created demo-project and build everything again.

```bash
cordova platform add android
cordova requirements
cordova build

# Note: this only works if an Android Emulator is running, or a Phone with the ADB enabled, is connected.
cordova run android
```

## Summary

Installing Cordova alone is straight-forward. Installing it with Android build/run-support is a bit more complicated. Especially if you don't know much about Android tooling. Sadly, the official Cordova documentation is barely touching the topic of installation. Also there close to no installation instructions for Red Hat distributions such as Fedora or CentOS available on the web. Hope this short article helps you out when you want to get your feet wet with Cordova. If you have any questions left, feel free to post a comment below. Have fun!
