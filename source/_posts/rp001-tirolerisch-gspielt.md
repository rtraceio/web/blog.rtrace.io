---
title: "[Ruffy presents 🎧 #001] Tschentig"
date: 2022-07-09
authors: 
    - raffael@rtrace.io
tags:
    - Audio
    - Music
    - Promo
    - Volksmusik
    - Austria
    - Tyrol
    - Ruffy presents
    - Tirolerisch G'spielt
    - Tschentig
categories:
    - Music
photos:
    - /images/artists/artists-tirolerisch-gspielt-logo.png
    - /images/artists/artists-tirolerisch-gspielt-band.png
toc: false
comments: true
---

`Tschentig`, formerly known as `Tirolerisch G'spielt`, convinces with young and cheeky austropop, showing without a doubt that its origin is rooted in typical tyrolean folk music.`Tschentig` has succeeded in combining classical austrian folk instruments with catchy melodies and beautiful lyrics. Swinging songs with lyrics taken from life. Songs to sing along. In this blog post, we will have a look on what `Tschentig` has to to offer.

<!-- more -->

## Tirolerisch G'spielt

### In a nutshell

`Tschentig`, formerly known as `Tirolerisch G'spielt`, convinces with young and cheeky austropop, showing without a doubt that its origin is rooted in typical tyrolean folk music.`Tschentig` has succeeded in combining classical austrian folk instruments with catchy melodies and beautiful lyrics. Swinging songs with lyrics taken from life. Songs to sing along. In this blog post, we will have a look on what `Tschentig` has to to offer.

![Tirolerisch G'spielt Band](/images/artists/artists-tirolerisch-gspielt-band.png)

The project was started in 2006. The foundation of their music was of course the Tyrolean folk music. Since folk music alone felt a little bit too monotonous to the band, they wanted to get their feet wet with new musical challenges. The result? They combined various musical genres and took influence from House and Pop music. While being modern with the arrangements, `Tschentig` interpretes their songs with the same traditional instruments, creating a wonderful balance between new and old. 

### Members

`Tschentig` is mostly known by `Hanna Maizner` as the voice of the band. Hanna is a very talented singer, songwriter and musician. In Austria Hanna was already able to make herself a name. She is well known as member of the band `Harfonie`. With `Harfonie`, she was able to celebrate one of her biggest successes on TV. She won "Die Große Chance" - a well known austrian TV talent show.

{% youtube N2Tk2R9xDfo %}

A typical instrument for Austrian folk music is a special type of diatonic harmonica (commonly referred to as: "Steirische Harmonika" or just "Steirische"). It is played by `Michael Angerer`. Michael is a music teacher at the musical school of Wattens in Tyrol, teaching the Steirische to his students.

Commonly heard throughout songs of `Tschentig` is the [Hackbrett](https://en.wikipedia.org/wiki/Hammered_dulcimer) (a type of a hammered dulcimer). It is played by `Daniel Maizner`, the brother of Hanna. Additionally to a Hackbrett you can hear Daniel playing the Trumpet, and singing on songs of `Tschentig`.

Finally, a good band requires a solid percussive foundation. This is where the father of `Daniel` and `Hanna` comes into play. `Paul Maizner` specializes in drums, cajon and other rythmic instruments. Sometimes one can hear Paul playing the [Double bass](https://en.wikipedia.org/wiki/Double_bass) throughout the songs.

### Frei wie der Wind

`Frei wie der Wind` was the first song I have discovered. The vocal performance of Hanna is even now fascinating me. Beautiful lyrics combined with a cheeky tyrolean dialect and traditional instruments are a guarantee for a pleasant listening experience. Hanna even does a light form of yodelling throughout the chorus which is really enjoyable

{% youtube Jhdi-gvVoco %}

### Zwoate Chance

`Zwoate Chance` is best described as: "traditional folk music, with a new pop makeover - with a 4 to the floor beat`. If you can hold your feet still, you might want to see a doctor.

{% youtube Wv1_9GUEJD8 %}

![Tirolerisch G'spielt](/images/artists/artists-tirolerisch-gspielt-logo.png)

## Links

- [Tschentig on YouTube](https://www.youtube.com/channel/UC8Li9euYJsBmV3abKQfohNQ)
- [Tirolerisch G'spielt, Tschentig Website](https://www.tirolerisch-gspielt.at)
