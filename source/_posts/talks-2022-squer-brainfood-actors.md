---
title: Why Distributed Systems Don't Need To Be Complicated
date: 2022-11-11
authors: 
    - raffael@rtrace.io
    - paul.rohorzka@squer.at
tags:
    - Akka
    - Microsoft Orleans
    - Actors
    - SQUER
    - Brainfood Talk
    - Distributed Systems
    - Architecture
    - Talk
    - Presentation
    - Vienna

categories:
    - Development

photos:
    - /images/talks/squer-brainfood-talks.webp

toc: false
comments: true
---

Today’s architectural challenges, especially in IoT scenarios, are often tackled using distributed systems with microservices as go-to solution of choice. While microservices offer great benefits, they also tend to increase the overall complexity of systems. Paul and me will show you how actor frameworks can mitigate those problems, using a long-forgotten paradigm from the early days of computer science.

<!-- more -->

## Hard facts

- 📆 When: 11th of November 2022, 1800 (UTC+1)
- 📍 Where: SQUER office, Marxergasse 17/1/33-34, 1030 Vienna, Austria 🇦🇹
- 📺 Live on [YouTube](https://www.youtube.com/@squersolutions)
- 📺 Recoding on [YouTube](https://www.youtube.com/watch?v=XTDgEF5alMc)
- 👨‍👩‍👧 Organized by: [SQUER](https://www.squer.at)

## Background

Paul and me worked together on projects in which we had the pleasure of exploring Actors, as a solution to make distributed systems a lot easier. We had been working with [Orleans](https://learn.microsoft.com/en-us/dotnet/orleans/overview) - a F(L)OSS Actor Framework maintained by Microsoft. Both of us were fascinated by the way Orleans nicely supports the Developer tackling the most complex distributed problems.

At a dinner together we came up with the idea to share our experiences with the world. Coincidentally Paul works for SQUER Solutions, a vienna-based company, that is very interested and active in sharing knowledge with people - especially at meetups. One of these meetups is the Brainfood-Talk over at the SQUER HQ. Brainfood-Talks are a series of events where people at SQUER talk about their field of action and interests. 

## The talk

So the plan was foiled: "Let's make a Brainfood Talk about Actors". Fast-Forward to the 11<sup>th</sup> of November and here's the actual recording of the talk.

{% youtube XTDgEF5alMc %}

## Further Links

- [Paul on Twitter](https://nitter.kavin.rocks/paulroho)
- [SQUER](https://squer.at)
- [SQUER Meetup Group](https://www.meetup.com/de-DE/squer-brainfood-talks/)