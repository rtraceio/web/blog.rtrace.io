---
title: '[🎶Release]: 2Phaze - Smash Up 0x01'
date: 2023-09-03
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - Mashleg
    - FL Studio
    - Drum & Bass
    - 2Phaze
    - Smash Up

categories:
    - Music Production

photos:
    - /images/albumart/albumart-2phaze-smash-up-0x01.jpg

toc: true
comments: true
---

It's time for a new release again 🚀🚀🚀. This time ruffy presents a tune combining catchy Drum & Bass and (lots of) iconic vocals from the previous 20 years. All that, combined into a single song. The project initially started out as a remix for "All That I Need" by `S3rl` and was further on extended with `Adele`, `Linkin Park`, `Jason Derulo`, `Ellie Goulding` `Dada Life` and `Candee Jay`.  At some point the amount of vocals took overhand, so I've decided to call it a "Mashleg" (basically a mixture out of bootleg and a mashup). Now this song marks the first Mashleg of a series of Mashlegs called `Smash Ups`. Come and have a listen...

<!-- more -->

## Introduction

It's time for a new release again. This time ruffy presents a tune combining catchy Drum & Bass and (lots of) iconic vocals from the previous 20 years. All that, combined into a single song. The project initially started out as a remix for "All That I Need" by `S3rl` and was further on extended with `Adele`, `Linkin Park`, `Jason Derulo`, `Ellie Goulding` `Dada Life` and `Candee Jay`.  At some point the amount of vocals took overhand, so I've decided to call it a "Mashleg" (basically a mixture out of bootleg and a mashup). Now this song marks the first Mashleg of a series of Mashlegs called `Smash Ups`.

## Background 

This project initially started out as a remix for S3rl. S3rl thankfully provided the world with the vocals of "All That I Need", and I felt the urge to work on a more DnB'ish project lately. And boom, a more DnB'ish project was started. A little later in the game I figured out, that the chord progression is quite common and parsed through my vocal library to find further candidates that could fit, Turned out, that lots of pop songs roughly follow the same chord progression. A little pitchy patchy here and there and and vocals nicely integrate into the arrangement. At some point the amount of random vocals in this song took overhand, so I've decided to call it a "Mashleg".

## Streaming

### Soundcloud

{% soundcloud track 1606208268 %}


### YouTube

{% youtube bW0EQ5tNjfI %}


## Download

| Track                      | Type    | Download-Link                                                       | Password    |
|:--------------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| Smash Up 0x01 (Radio Edit) | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/8MQGF2S8Y8#2isOWMdOOIPa) | `rtrace.io` |

## Have Fun

Download of the track is FREE. I really hope you enjoy this song! If you want to give me feedback on this `Smash Up` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io) or a leave a comment below.

## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)