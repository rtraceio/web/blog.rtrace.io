---
title: '[🎶Release]: Ruffy Le RaRe & A.R. - Rather Be'
date: 2023-12-30
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Music
    - Release
    - FL Studio
    - Electro
    - House
    - Deep House
    - Ruffy Le RaRe
    - Rather Be

categories:
    - Music Production

photos:
    - /images/albumart/albumart-ruffy-le-rare-rather-be.png

toc: true
comments: true
---

The year 2023 slowly but steadily comes to an end. And with it a new `Ruffy Le RaRe` release comes along to end the year with a "bang". Ruffy partnered up with A.R. and created a Deep House re-interpretation of "Rather Be" by Jess Glynne and Clean Bandit. This cover contains acoustic guitars, crunchy vocals and jazzy synth leads combined with a steady four-to-the-floor house rhythm. You've probably heard the original in the radio for at least 3 million times already, yet this version will be like listening to a completely different song.

<!-- more -->

## Introduction

The year 2023 slowly but steadily comes to an end. It was a challenging year with many roadblocks in the way, yet for me it was one of the most successful years util now - both from a professional and personal perspective. Finished up my masters degree, learned lots of new things, got to know many inspiring personalities, and sharpened my music production skills. All in all, a great year! And with the end of this year also a new `Ruffy Le RaRe` release comes along to leave 2023 with a "bang". Ruffy partnered up with `A.R.` and created a Deep House re-interpretation of "Rather Be" by Jess Glynne and Clean Bandit. This cover is special because it contains many acoustic elements - such as acoustic guitars, crunchy vocals and jazzy synth leads, and all that combined with a steady four-to-the-floor house rhythm. You've probably heard the original in the radio at least 3 million times already, yet this version will be like listening to a completely different song.

![CoverArt](/images/albumart/albumart-ruffy-le-rare-rather-be.png)

Aside from the music, there was also some great work on the cover art of the song! KUDOS and huge `THANK YOU` go out to [Le fractal](https://mstdn.social/@lefractal) for taking the the time and create this wonderful artwork. Really appreciate it!

## Downloads

| Track                  | Type    | Download-Link                                                       | Password    |
|:----------------------:|:-------:|:-------------------------------------------------------------------:|:-----------:|
| Rather Be (Radio Edit) | HQ FLAC | [ProtonDrive](https://drive.proton.me/urls/MYG6W05WD8#eUfK6NifMnqM) | `rtrace.io` |


## Streams

{% soundcloud track 1703694750 %}

{% youtube AZ9mvK8aIz4 %}

### Streaming sites

- ![Soundcloud](/img/soundcloud-16x16.png) [Soundcloud](https://soundcloud.com/djraremusic/ruffy-le-rare-ar-rather-be-radio-edit)
- ![Funkwhale](/img/funkwhale-16x16.png) [Funkwhale](https://am.pirateradio.social/library/tracks/76320)
- ![YouTube](/img/youtube-16x16.png) [YouTube](https://www.youtube.com/watch?v=AZ9mvK8aIz4)


## Lyrics

```text
Oh, oh, oh

We're a thousand miles from comfort
We have travelled land and sea
But as long as you are with me
There's no place I'd rather be

I would wait forever
Exalted in the scene
As long as I am with you
My heart continues to beat

With every step we take
Kyoto to The Bay
Strolling so casually
We're different and the same
Gave you another name
Switch up the batteries

If you gave me a chance I would take it
It's a shot in the dark but I'll make it
Know with all of your heart, you can't shame me
When I am with you, there's no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
Ooh

We set out on a mission
To find our inner peace
Make it everlasting
So nothing's incomplete

It's easy being with you
Sacred simplicity
As long as we're together
There's no place I'd rather be

With every step we take
Kyoto to The Bay
Strolling so casually
We're different and the same
Gave you another name
Switch up the batteries

If you gave me a chance I would take it
It's a shot in the dark but I'll make it
Know with all of your heart, you can't shame me
When I am with you, there's no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
When I am with you, there's no place I'd rather be, yeah

Be
Ooh
Be, be, be, be, be, be, be, be, be
Yeah-e-yeah-e-yeah-e-yeah-e-yeah, yeah!

If you gave me a chance I would take it
It's a shot in the dark but I'll make it
Know with all of your heart, you can't shame me
When I am with you, there's no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
No, no, no, no, no, no place I'd rather be
When I am with you there's no place I'd rather be
``` 

## Have Fun

Downloads of the tracks are FREE. I really hope you enjoy this song! If you want to give me feedback on `Rather Be` (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io).

## Links

- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)