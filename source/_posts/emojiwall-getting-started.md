---
title: Getting started with your own emojiwall
date: 2022-11-20
authors: 
    - raffael@rtrace.io
tags:
    - smol.stream
    - OBS
    - Streaming
    - Owncast
    - FLOSS
    - emojiwall
    - Self-Hosting

categories:
    - Streaming

photos:
    - /images/software/emojiwall/smolstream-emoji-logo.png

toc: true
comments: true
backgroundtheme: emojiwall
---

Are you a streamer? Are you into Emojis? Do you want your community to engage more in your chat? Then say no more, you're right here! In this post I'll show you the awesome F(L)OSS project called `emojiwall`. In a nutshell, it's a fun enhancement for your stream, constantly having a look for emojis in chat messages. When a new message with emojis comes in, it extracts the emojis and makes them fly through the stream. We'll cover how to configure it using [smol.stream](https://smol.stream/emojiwall), and talk about how to host your own `emojiwall`, as well as some best-practices tips & ticks and some hacks. 🤯🎪🚶🏗️❣️👯‍♂️🛢️🎵🈚🛫🐲🎂😺🐝🍖🍦🧟‍♀️☺️🎩⛰️👔🎢🧡🧜‍♀️🏓♑👨‍👩‍👦‍👦🤩🌭🕥🙆‍♀️💬🏊‍♂️😌♿🅱️🎙️🧘‍♂️💟♀️🥛*️⃣🔔👠👨‍👩‍👦🌀🎫🕹️👷‍♀️👱‍♀️🚺

<!-- more -->

## What is the emojiwall?

The first question you might have, is: "What the hell is an `emojiwall`". Just have a look at the background. Can you see these emojis bubbling up from the bottom of your screen to the top in the background? Yes? Congratulations, you see an `emojiwall` in action. In a nutshell, an `emojiwall` takes a source of emojis (e.g., hardcoded, or from chat-messages that contain emojis) and makes them fly through your screen.

### What would I need this for?

If you're a streamer, then `emojiwall` is a powerful tool to increase the engagement of your community. You make it a lot more fun for people to write-chat messages with emojis. Especially if you're using Owncast with custom emotes.

The `emojiwall` can even be helpful to you if you're not a streamer. Are you looking for a small gimmick on your website? Maybe spice it up a little bit for a special event, or maybe for an April Fool's joke? For whatever reason you want an `emojiwall` on your website (just like I wanted on this site), the `emojiwall` got your back.

### How does an emojiwall work?

![emojiwall with smol.stream overview](/images/software/emojiwall/smol-stream-overview.drawio.svg)

Sounds complicated? No, not at all. Let me explain.

- activate a browser overlay in your OBS that points to an `emojiwall`-server.
- configure Owncast webhooks to notify the `emojiwall`-server of new messages with emojis
- let your community engage in the chat and make them write silly emojis
- and voilá - emojis everywhere on the screen

## Introducing [smol.stream](https://smol.stream/emojiwall)

![smol.stream logo](/images/software/emojiwall/smolstream-emoji-logo.png)

[smol.stream](https://smol.stream/emojiwall) is one of the awesome projects of [Le fractal](https://mastodon.social/web/@lefractal@mstdn.social). `smol.stream's` mission is to provide various services and resources for "smol" streamers, the first one being the `emojiwall`. While you can host the `emojiwall` by yourself (see [Self-Hosting the emojiwall](#Self-Hosting-the-emojiwall)), `smol.stream` makes it a lot easier for you to get started with it. When visiting the site, you'll be greeted with instructions on how to configure an `OBS` browser overlay bound to your Owncast Server. This is by far the easiest way to get you started straight away. The heavy lifting for setting up the `emojiwall`-server is done for you already, so you can just enjoy all the benefits of a fully managed `emojiwall` on your stream. However, a little bit of configuration - to integrate the emojiwall into your streaming-setup - is still necessary.

### Getting an emojiwall-URL

First we need to get ourselves a secret emoji-token by visiting [smol.stream](https://smol.stream/emojiwall). In my example I got `🐠👩‍🚀🚜☢️🎿🤶🇰🇵🍮🛵🐧` as secret emoji-token. Don't share this token with anyone. Please also make sure NOT to copy the token I have used in this example - it won't work for you. Next, enter your hostname/fqdn (the address where your Owncast-Server) is hosted/reachable. Since I host my Owncast-Instance at [https://live.rtrace.io](https://live.rtrace.io), I entered `https://live.rtrace.io`. This is important if you want your `emojiwall` to also show custom-emotes of your Owncast server. Either way, make sure to include `https://` or `http://` as scheme-prefix, as well as any non-standard TCP-Portnumber if necessary.

> ![Owncast Browser Overlay URL](/images/software/emojiwall/smol.stream-browser-overlay.png)

### Configuring OBS

Now it's time to go ahead and configure OBS to use `smol.stream` through our freshly generated token. So we'll fire up `OBS` and add a new Source with the `➕` symbol in the sources section. Heavily simplified the `emojiwall` is just a website, so to allow OBS to show the emojiwall, we just need to add a "Browser" as source.

> ![adding a browser source in OBS](/images/software/emojiwall/obs-add-browser-overlay-p1.png)

Now we copy the URL generated from `smol.stream` into the URL field of our Browser Source. Don't worry if the URL you pasted does look slightly different than in your browser. OBS might not support all emojis on your operating system yet. Even though the URL does look broken, it's fine - trust me.

> ![configuring the browser source in OBS](/images/software/emojiwall/obs-add-brower-overlay-p2.png)

In case the Custom CSS is not set for you, make sure to set it yourself. Simply copy-paste the following CSS lines, that make the browser overlays background transparent.

```css
body {
  background-color: rgba(0, 0, 0, 0);
  margin: 0px auto;
  overflow: hidden;
}
```

That's it. OBS is configured and ready to rock some emojis. Half of the hard work is done. Now it's time to configure Owncast to notify the `emojiwall` when new chat-messages arrive.

### Configuring Owncast Webhooks

[Owncast](https://owncast.online) luckily supports "Custom Integrations". Such integrations make Owncast fire events when certain things happen. These events are often called "Webhooks". And with such a webhook we now can notify the `emojiwall`, that a new messagearrived in the Owncast chat.

Head over to your Owncast administration panel, navigate to "Integrations" > "Webhooks" (alternatively click the `/admin/webhooks`-link in the smol.stream insructions) and then click the "**Create webhook**"-button. Now all that's left to do is to paste the URL that `smol.stream` gave us, into the webhook settings. Make sure to activate the webhook to only fire "When a user sends a chat message". Finally, click "Ok" and you're ready to use the `emojiwall`.

> ![Owncast Browser Overlay URL](/images/software/emojiwall/owncast-add-webhook.png)

### Time to toy around with your emojiwall

Start your stream and smash some emojis into the chat! If you have configured everything as described above, you'll immediately see the emojis you've typed flying through your stream! Congratulations 🥳🥳🥳! The only thing left to say here is "Have fun with `smol.stream` and your own emojiwall", unless of course you prefer to host the `emojiwall` yourself. Then please continue reading.

## Self-Hosting the emojiwall

If you prefer to self-host the `emojiwall`-server, your options are either running it containerized with `podman` (or optionally `docker`) or installing it natively on your server. The following architectural diagram shows you the required components to get started.

![emojiwall self-hosting overview](/images/software/emojiwall/emojiwall-selfhosting-overview.drawio.svg)

### containerized emojiwall

The `emojiwall` is published as container-image to [quay.io](https://quay.io/repository/smolstream/emojiwall). It is built from the [original repository](https://framagit.org/owncast-things/owncast-emojiwall). If you prefer, you can build your own container image from the [Dockerfile](https://framagit.org/owncast-things/owncast-emojiwall/-/blob/main/Dockerfile) in the repository.

```bash
podman container run \
    --name "mycoolemojiwall"
    --detach \
    -p 5000:5000 \
    -e HOSTNAME='smol.stream' \
    -e PREFIX='/emojiwall' \
    -e SCHEME='http' \
    -e PORT='5000' \
    -e PUBLIC_PORT='5000' \
    quay.io/smolstream/emojiwall
```
The container can be configured with environment-variables. The following environment variables are configurable.

- **HOSTNAME** sets a publicly exposed hostname
  - if you want to test it locally, set it to `localhost`
  - if you want to host it accessible from the public, set it to your FQDN (fully qualified domain name)

- **PREFIX** (optional) sets a path prefix for the URL (e.g., `/myAwesomeEmojiWall`)
  - if you want to access the emojiwall directly without prefix, leave it empty
  - if you want the `emojiwall` to be accessible from a subdirectory set it to `/<YourSubdirectoryName>`

- **SCHEME** whether the emojiwall is shipped via TLS/SSL-secured **HTTPS** or via plain **HTTP**
  - set it to 'https', if you expose the `emojiwall` through a reverse-proxy that terminates TLS
  - set it to 'http', if you directly access the emojiwall (e.g. from localhost)
  - if you run a public/shared instance please consider utilizing a proper HTTPS setup
  - the `emojiwall` does not support HTTPS directly. This can be done through reverse-proxy

- **PORT** sets the (TCP-) port the `emojiwall` is listening on

- **PUBLIC_PORT** sets the (TCP-) port the `emojiwall` is served through (e.g. public port of a reverse-proxy)
  - if the `emojiwall` is served through a reverse-proxy the listen is not directly exposed
  - the emojiwall is using this port to rewrite URLs for static content to the public facing port
  - most likely (tcp/80 and/or tcp/443 - however can be a custom port as well)
  - if you're directly accessing the `emojiwall`, just use the same port entered previously at "**PORT**"


### Installation from sources

The `emojiwall` can easily be installed manually from sources, by simply cloning the repository to your machine. The project is written with [node.js](https://nodejs.org/en/) and the awesome [express](https://expressjs.com/) web-framework. So the only prequisite is `npm` and `node` pre-installed. The `emojiwall` does not require a specific `node.js` version - so installing node from your distribution package manager is possible too.

#### Installation

```bash
git clone https://framagit.org/owncast-things/owncast-emojiwall.git
cd ./owncast-emojiwall
npm install
```

#### Configuration

Running the `emojiwall` requires you to have a look into its configuration. The configuration can be found in the file `./config/default.json`. Here's an example configuration.

```json
{
  "webhooks": {
    "host": "emojiwall.rtrace.io",
    "prefix": "",
    "scheme": "http",
    "port": "5000",
    "public_port": "443"
  }
}
```
- **host** sets a publicly exposed hostname
  - if you want to test it locally, set it to `localhost`
  - if you want to host it accessible from the public, set it to your FQDN (fully qualified domain name)

- **prefix** (optionally) sets a path prefix for the URL (e.g. `/myAwesomeEmojiWall`)
  - if you want to access the emojiwall directly without prefix, leave it empty
  - if you want the `emojiwall` to be accessible from a subdirectory set it to `/<YourSubdirectoryName>`

- **scheme** whether the emojiwall is shipped via TLS/SSL-secured **HTTPS** or via plain **HTTP**
  - set it to 'https', if you expose the `emojiwall` through a reverse-proxy that terminates TLS
  - set it to 'http', if you directly access the emojiwall (e.g. from localhost)
  - if you run a public/shared instance please consider utilizing a proper HTTPS setup
  - the `emojiwall` does not support HTTPS directly. This can be done through reverse-proxy
  
- **port** sets the (TCP-) port the `emojiwall` is listening on
  - the only requirement is that this port is accessible
  - be aware that for privileged ports (e.g., tcp/80, tcp/443), the `emojiwall` requires to be run as `root`

- **public_port** sets the (TCP-) port the `emojiwall` is served through (e.g. public port of a reverse-proxy)
  - if the `emojiwall` is served through a reverse-proxy the listen is not directly exposed
  - the emojiwall is using this port to rewrite URLs for static content to the public facing port
  - most likely (tcp/80 and/or tcp/443 - however can be a custom port as well)
  - if you're directly accessing the `emojiwall`, just use the same port entered previously at "**port**"

#### Starting

After all the dependencies of the `emojiwall` were installed, it's time to start our `emojiwall`. Navigate into the root-directory of the repository and invoke `node`.

```bash
node index.js
```

Your `emojiwall` is now successfully started and is listening on the configured TCP-port. When you see log-output that the `emojiwall` is now listening on the configured port, the `emojiwall` is ready to be tested. Fire up your browser of choice, and visit `http://localhost:5000` (assuming you configured the listening port to port 5000 and kept the prefix setting empty).

## Manually sending emojis to your emojiwall

The `emojiwall` is open enough, to not require you to use Owncast at all. You may also manually send emojis to an emojiwall, which makes it perfectly suitable for usage in non-live setups (e.g. OBS recordings). Since the `emojiwall` just listens for incoming webhooks, we can just manually invoke such webhooks ourselves. 

The most lightweight webhook body that is `HTTP POST`'ed from Owncast to the emojiwall looks like this:
```json
{
  "eventData": {
    "body": "❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️"
  }
}
```

This allows you to send emojis to the emojiwall from anywhere.
Here an example with `curl` that allows you to send emojis right from your terminal. 
```bash
curl -X POST --header "Content-Type: application/json" \
     --data '{"eventData":{"body":"😀💙"}}' $EMOJIWALL_URL
```

Alternatively you can also use graphical REST-Clients like [Insomnia REST-Client](https://insomnia.rest/)

## Customizing your Emojiwall

There are a few things you can customize, such as the size and speed of the emojis, by adding parameters to the URL you use to setup the browser source in OBS Studio. The following parameters are supported:

- `minsize`: The minimum size of the emojis in pixels.
- `maxsize`: The maximum size of the emojis in pixels.
- `mintime`: The minimum time the emojis take to cross the screen, in seconds.
- `maxtime` The maximum time the emojis take to cross the screen, in seconds.
- `maxcount`: The maximum number of emojis to display at the same time.
- `effect-rotation`: Set to 1 to make emojis slightly rotate as they travel up the screen. Depending on your configuration and the number of emojis, this can be ressource intensive — but it looks great!


## Final Words

If you run into troubles and need help, please feel free to ask for support either via mail [support@smol.stream](mailto:support@smol.stream) or use the comment-section below. Additionally, if you're a fan of the `emojiwall` (and/or `smol.stream`) make sure to send a *smol* "Thank You" to [Le fractal](https://mastodon.social/web/@lefractal@mstdn.social). He invested lots of time and effort into the `emojiwall`. But even more important, please share `smol.stream` and the `emojiwall` with your friends and people that might be interested - let's make the web a little brighter with funny emojis flying through screens of people.

## Links & Further Resources

- [smol.stream](https://smol.stream/emojiwall)
- [Framagit Repository](https://framagit.org/owncast-things/owncast-emojiwall)
- [Quay.io Container Registry](https://quay.io/repository/smolstream/emojiwall)
- [Emojiwall NPM Package](https://www.npmjs.com/package/emojiwall/)
- [Le fractals Owncast instance](https://direct.disquette.top)
- [Le fractal in the Fediverse](https://mstdn.social/@lefractal)
