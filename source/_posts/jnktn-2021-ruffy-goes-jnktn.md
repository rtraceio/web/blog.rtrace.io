---
title: I am resident DJ at Jnktn.TV 🔥
date: 2021-06-05
authors: 
    - raffael@rtrace.io
tags:
    - Streaming
    - Audio
    - Video
    - Music
    - Owncast
    - Jnktn.TV
    - DJ

categories:
    - Streaming
    - Music

photos:
    - /images/jnktn/jnktn-logo.png
    - /images/memes/success-kid.jpg

toc: true
comments: true
---

I was 15 years old when I first played my music on a small Austrian web radio called `beatnight.at`. All I had back then, was a `Reloop Digital Jockey 2` and [VirtualDJ](https://www.virtualdj.com/) to mix music. I'll have to admit, that the DJ sets back then weren't really professional, however this experience sparked my interest in (web-)radio stations. Since the beginning of June 2021, I'm a member of the [Jnktn.TV Team](https://jnktn.tv) and I'll be streaming on Saturdays on a bi-weekly basis. My show is called `Dance Attack with Ruffy`, and - as the name implies - it's all about dance music. I'll mix the latest and the best songs out of House, Electro, Dance, Hands Up, Hardstyle and Hardcore live for you.

<!-- more -->

## Introduction

I was 15 years old when I first played my music on a small Austrian web radio called `beatnight.at`. All I had back then, was a `Reloop Digital Jockey 2` and [VirtualDJ](https://www.virtualdj.com/) to mix music. I'll have to admit, that the DJ sets back then weren't really professional, however this experience sparked my interest in (web-)radio stations. Since the beginning of June 2021, I'm a member of the [Jnktn.TV Team](https://jnktn.tv) and I'll be streaming on Saturdays on a bi-weekly basis. My show is called `Dance Attack with Ruffy`, and - as the name implies - it's all about dance music. I'll mix the latest and the best songs out of House, Electro, Dance, Hands Up, Hardstyle and Hardcore live for you.

## Why?

The thing I love most about making music, is the fact that at some point, someone will stumble over your work and enjoy every single second. It's even more fascinating that you can raise the mood of people and help them to break out of their everyday life for a moment. Playing / DJ'ing music on a live stream is a great way to remotely connect to your audience and achieve exactly that. That was the reason why I started my own Owncast instance on [live.rtrace.io](https://live.rtrace.io) in February 2021. After the first few sets there [@meisam](https://fosstodon.org/@meisam) - who joined some of my streams - introduced me to Jnktn. I immediately fell in love with the project, its variety of content and its people. Needless to say, I badly wanted to be a part of this awesome project. From there on, it just took a few mails and a brief casting with the big boss of Jnktn - and I was accepted!

![Success Kid Meme](/images/memes/success-kid.jpg)

## What's Jnktn.TV?

> Jnktn.TV is an online streaming platform which makes use of and promotes open-source software. The aim is to create a community project which brings together a variety of content creators so as to build a platform that can help entertain and engage. We're live Saturday evenings (UTC+1) for the moment but plan to expand this when possible with our ever increasing number of shows.

Jnktn is (as of writing) a small project with a handful of streamers, based in the UK. It's a very ambitious project with nice people and even nicer content. Shows are mostly music shows, but there's a variety of cool things to see - for example `Get ready with Andrina`, where the host of the show (Andrina) guides you through her make-up procedure for the evening. The project is run by [@jumboshrimp](https://mastodon.online/@jumboshrimp). He takes care of the server maintenance, arranging the schedules for the coming weeks and he's also hosting some of the sessions there. Jnktn.TV offers you the best entertainment for the day - I promise!

### What's being played?

as I already mentioned, there is plenty of awesome content on Jnktn. I just want to highlight a few of them.

- **Gary's Citrus Club**
I wasn't aware of how much I enjoy Rock music until I saw the Citrus Club with Gary! Every show is pure entertainment - especially since Gary has this lovely scottish accent.

- [**jumboshrimp's Radio Show**](https://mixcloud.com/jumboshrimp_edi)
Every show with Alex is an absolute ear-candy. Every show is a surprise maybe funk'ish House, maybe 80s, maybe something completely different?

- **Vera's Radio Show**
Each time I listen, I get the feeling of nostalgia for a time I wasn't even alive. It's really weird. oldskool Rock & Funk tunes all the way. There's [one particular show](https://www.mixcloud.com/Jnktn_TV/veras-radio-show-03-07-21/) with lots of cool irish/highland/celtic tunes I want to point out.

- **Unpopular Stream**
Two words: Amazing video footage, and amazing music! (technically that was more than 2 words, I know). [@meisam](https://fosstodon.org/@meisam) plays cool underground'ish tunes in all different kind of genres like IDM, Darkstep, Dubstep, Drum & Bass, Noise and Ambient. Not only eye-candy, also ear-candy.

- [**Worky Sets**](https://www.mixcloud.com/worky/)
Did you know Rollerskate-Discos exist? No? Me neither, until I saw a Worky Live Set from a rollerskate disco. Worky is an excellent DJ with excellent tunes. House'ish, Dance'ish, Oldskool'ish - something in there for everyone!

- **Get Ready With Andrina**
Well, let's just say I learned a lot about make-up so far. Even if you'll never put make-up on it's still fascinating how much effort one can put into make-up. Oh btw. also the music is nice.

## Dance Attack with Ruffy

I host a show called `Dance Attack with Ruffy` on Saturdays, usually on a bi-weekly basis. You'll hear the best of Hands Up, House, Electro, Dance, Trance, Drum & Bass, Dubstep, Psytrance, Hardtrance, Hardcore, Happy Hardcore, Trap, Future Bass and much much more. I'll be announcing my shows a few days beforehand on [Mastodon](https://mastodon.social/@rarepublic). Just make sure to get your dancing shoes ready before the show begins. Just in case you missed a show, the recordings of the shows are usually uploaded to [Mixcloud](https://www.mixcloud.com/jnktn_tv/) a few days afterwards. I also plan to mirror the show recordings here at some point. If you have any wishes for tunes to play or a specific genre you'd like to hear just message me - we'll arrange something. Promised!

## Where?

Well it's easy. Just open your browser and navigate to [Jnktn.TV](https://jnktn.tv) and press the play button. You'll find the Schedule on the bottom of the page. You might even create an account on the chat-platform to reserve your nickname.

### Jnktn.TV on Social Media

if you have an account on any of these, it'd be great if you could leave a "Follow" there. Jnktn has earned it

- [Mastodon](https://mastodon.online/@jnktn_tv)
- [Instagram](https://www.instagram.com/jnktn.tv/)
- [Bibliogram (privacy friendly Instagram Frontend)](https://bibliogram.rtrace.io/u/jnktn.tv)
- [Mixcloud](https://www.mixcloud.com/jnktn_tv/)

### Others

- [Jnktn.TV Sourcecode on Codeberg](https://codeberg.org/jnktn.tv)
- [TeeMill Merch Store](https://jnktn.teemill.com/)
- [Buy Jnktn.TV a coffee on Ko-Fi](https://ko-fi.com/jnktntv)
- [Donate via LiberaPay](https://liberapay.com/Jnktn.tv/donate)
- [Donate via OpenCollective](https://opencollective.com/jnktn-tv)

![Jnktn.TV](/images/jnktn/jnktn-logo.png)
