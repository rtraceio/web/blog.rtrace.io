---
title: '[🎶Release]: Unknown Paradigm & Cold'
date: 2021-08-03
authors: 
    - raffael@rtrace.io
tags:
    - Production
    - Audio
    - Video
    - Music
    - Release
    - Remix
    - FL Studio
    - FLP
    - Hands Up
    - UK Hardcore
    - 2Phaze
    - Unknown Paradigm
    - Cold
    - Timmy Trumpet

categories:
    - Music Production

photos:
    - /images/albumart/albumart-2phaze-unknown-paradigm.jpg

toc: true
comments: true
---

Over the last few years I had very little time for music - especially for my own music. University here, work there, friends, family on the other side - trust me, it's not always easy to actually have time for producing music. Even if you have a few spare minutes left, sometimes the creativity-levels are just below the required threshold. As a result, I started lots of projects during that time, but didn't finish a single one. My project folder now contains round about 50 unfinished ideas, that are just waiting for being started/finished. Well, and exactly that had to stop! I rolled up my sleeves and got to work with the goal to reduce the number of `WIP` projects. To celebrate this, I'm releasing my remix for `Timmy Trumpet - Cold` and a `2Phaze` original tune called `Unknown Paradigm`. But wait, there's even more. I'm also releasing the FLPs (Fruity Loops Project Files) for both projects, just to give back to the community, producers and everyone interested in my music.

<!-- more -->

## Introduction

Over the last few years I had very little time for music - especially for my own music. University here, work there, friends, family on the other side - trust me, it's not always easy to actually have time for producing music. Even if you have a few spare minutes left, sometimes the creativity-levels are just below the required threshold. As a result, I started lots of projects during that time, but didn't finish a single one. My project folder now contains round about 50 unfinished ideas, that are just waiting for being started/finished. Well, and exactly that had to stop! I rolled up my sleeves and got to work with the goal to reduce the number of `WIP` projects. To celebrate this, I'm releasing my remix for `Timmy Trumpet - Cold` and a `2Phaze` original tune called `Unknown Paradigm`. But wait, there's even more. I'm also releasing the FLPs (Fruity Loops Project Files) for both projects, just to give back to the community, producers and everyone interested in my music. Note aside: Projects just require vanilla `FL Studio` (no external plugins required).

## Tracks

### 2Phaze - Unknown Paradigm

| Track                                                  | Type    | Download-Link                                                            | Password    |
|:------------------------------------------------------:|:-------:|:------------------------------------------------------------------------:|:-----------:|
| 2Phaze - Unknown Paradigm (Original Extended Mix)      | HQ FLAC | [ProtonDrive](https://drive.protonmail.com/urls/ZGB6W4ENHR#FQze6Bl4Ph53) | `rtrace.io` |
| 2Phaze - Unknown Paradigm (Original Extended Mix)      | FLP/ZIP | [ProtonDrive](https://drive.protonmail.com/urls/5RPVBDX370#R2kSPw7k5ss2) | `rtrace.io` |

#### Extended Club Mix

{% invidious l6X_XAV9Hk4 %}

{% youtube track 1099278472 %}

#### Streaming sites

- ![iTunes](/img/itunes-16x16.png) [Apple Music](https://music.apple.com/us/album/unknown-paradigm/1646493901)
- ![Spotify](/img/spotify-16x16.png) [Spotify](https://open.spotify.com/album/292bVWKKqJPKZqk2TMx2Gm)
- ![Deezer](/img/deezer-16x16.png) [Deezer](https://www.deezer.com/de/album/357870087)
- ![Amazon Music](/img/amazon-music-16x16.png) [Amazon Music](https://music.amazon.com/albums/B0BFP398KS)
- ![Soundcloud](/img/soundcloud-16x16.png) [Soundcloud](https://soundcloud.com/djraremusic/unknown-paradigm)


### Timmy Trumpet - Cold (2Phaze UK Hardcore Remix)

| Track                                                  | Type    | Download-Link                                                            | Password    |
|:------------------------------------------------------:|:-------:|:------------------------------------------------------------------------:|:-----------:|
| Timmy Trumpet - Cold (2Phaze UK Hardcore Radio Edit)   | HQ FLAC | [ProtonDrive](https://drive.protonmail.com/urls/8K4ZCRZASM#qvPGvfbwTimk) | `rtrace.io` |
| Timmy Trumpet - Cold (2Phaze UK Hardcore Extended Mix) | HQ FLAC | [ProtonDrive](https://drive.protonmail.com/urls/HK08NRXGB0#Zwu48G89bEWB) | `rtrace.io` |
| Timmy Trumpet - Cold (2Phaze UK Hardcore Remix)        | FLP/ZIP | [ProtonDrive](https://drive.protonmail.com/urls/6KKCQDHJGC#8MP6BtvTZQMK) | `rtrace.io` |

#### Extended Club Remix

{% youtube e0565-d5tZw %}

{% soundcloud track 1098705142 %}

#### Radio Edit

{% soundcloud track 1098710812 %}

## Have Fun

Downloads of the tracks are FREE, also the FLPs are free to download for everyone interested. I really hope you enjoy those two tracks. If you want to remix/mashup/edit/bootleg my tracks, please feel free to do so - just keep in mind to send me a mail with your results. Would be happy to hear what you came up with. Also, if you want to give me feedback on those tracks (which is obviously highly appreciated) - please send a mail to [feedback@rtrace.io](mailto:feedback@rtrace.io).

## Links

- [Timmy Trumpet on YouTube](https://www.youtube.com/user/timmytrumpettv)
- [Timmy Trumpet Website](https://www.timmytrumpet.com/)
- [Spinnin Records](https://spinninrecords.com/)
- [Ruffy on Soundcloud](https://soundcloud.com/djraremusic)
- [Ruffy on YouTube](https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ)
- [Ruffy on Funkwhale](https://am.pirateradio.social/@ruffy/)
- [2Phaze on ReverbNation](https://www.reverbnation.com/2phazemusic)
- [2Phaze on Bandcamp](https://2phaze.bandcamp.com)
- [2Phaze on Amazon Music](https://music.amazon.com/artists/B088F2KQD1/2phaze)
- [2Phaze on Apple Music](https://music.apple.com/us/artist/2phaze/358947385)
- [2Phaze on Deezer](https://www.deezer.com/de/artist/9596504)