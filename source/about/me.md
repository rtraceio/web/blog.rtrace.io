---
title: $whoami
authors: 
    - raffael@rtrace.io
tags:
    - Raffael
    - Rehberger
    - About Me
    - Über Mich
    - Software
    - Developer
    - Linux
    - Music
    - DJ
    - FLOSS
    - Fronius
    - FH Joanneum
    - UAS Joanneum
    - Upper Austria
    - Oberösterreich
    - Marchtrenk
    - 2Phaze
    - Ruffy Le RaRe
---

**Hi**, I’m Ruffy (or Raffi for my German-speaking friends). I’m about 0x1E years old and I come from a charming small city near Linz, Austria. As a software developer with a deep passion for clean code, I have a strong focus on backend development and (software-)architecture - UI/UX design? Not quite my thing, unless we’re talking TUIs (Terminal User Interfaces).

<!-- more -->

```python
person = Person(name='Raffael', nick='Ruffy', age=0x1A, region='Upper Austria')
person.set_interests(['Linux', 'Security', 'Clean-Code', 'Music'])
person.set_profession(job='Team Lead', company='Fronius International GmbH')
person.print_information()
```

**Hi**, I’m Ruffy (or Raffi for my German-speaking friends). I’m about 0x1E years old and I come from a charming small city near Linz, Austria. As a software developer with a deep passion for clean code, I have a strong focus on backend development and (software-)architecture - UI/UX design? Not quite my thing, unless we’re talking TUIs (Terminal User Interfaces).

To balance all those hours spent in front of a screen, I’m heavily into music - whether I’m playing, producing, or just listening. I play instruments like piano, clarinet, drums, and more, but these days, I mostly create music in DAWs (Digital Audio Workstations). My style leans towards electronic genres like Hands Up, Hardstyle, House, Dance, Electro, Dubstep, Drum & Bass, Hardcore, but I don't want to restrict myself to those. Feel free to check out [my latest work](/about/my/music)!

Fun fact: I sometimes ghost-produce for well-known Austrian and German DJs or assist musicians with their studio recordings. So, there’s a good chance you’ve heard my work without even realizing it! If you’re interested in ghost productions, studio recording sessions, or want to collaborate on a project, don’t hesitate to reach out to me at [collab@rtrace.io](mailto:collab@rtrace.io).

<img class="centered-profile-img" src="/images/rtraceio-raffael-roundbadge-1024x1024.png" alt="Ruffy" />

By day, I lead a team of 10 talented engineers at Fronius International GmbH, where I work in the Solar Energy division. My team and I develop a suite of services and applications that enable the secure remote management of DERs (Decentralized Energy Resources). These tools play a key role in keeping our electrical grids stable and operational while supporting the sustainable expansion of renewable energy. I began my professional journey through an apprenticeship in applied electronics, primarily working in R&D with a focus on embedded software development. Over time, I shifted from low-level embedded programming and hardware to backend .NET development in the cloud for [Solar.Web](https://solarweb.com). In 2018, I served as the lead developer for the project I now oversee as a Team Lead.

Besides work, I also completed both my bachelor’s and master’s degrees part-time at the [University of Applied Sciences Joanneum](https://fh-joanneum.at), where I specialized in Software Design (BSc) and IT & Mobile Security (MSc).

In my spare time, I enjoy going for walks, biking, building/hacking/breaking things, discovering new music and artists, or simply browsing my favorite image boards for some quality memes. I contribute to various F(L)OSS projects whenever I can, and occasionally DJ on live streams like [Jnktn.TV](https://jnktn.tv) and [live.rtrace.io](https://live.rtrace.io).
0 (and yes, I’m currently using Fedora, after years of Arch)

![my vast universe of interests rendered in a little word-cloud](/images/wordcloud.svg)

## Contact Me

- [raffael@rtrace.io](mailto:raffael@rtrace.io) | use [PGP](/about/my/pgp)

## Exclaimer

Blogs, Posts, Toots and other opinions/views/standpoints found here (and on any of my social media) are my own. They might not represent my employer(s) opinions/views/standpoints.

<script type="application/ld+json">
{
    "@context": "http://www.schema.org",
    "@type": "Person",
    "@id": "https://blog.rtrace.io/about/me",
    "name": "Raffael Rehberger",
    "alternateName": "Ruffy Le RaRe",
    "nationality": "Austrian",
    "birthPlace": {
        "@type": "Place",
        "address": {
            "@type": "PostalAddress",
            "addressLocality": "Wels",
            "addressRegion": "Upper Austria",
            "addressCountry": "Austria"
        }
    },
    "affiliation": [
        {
            "@type": "Organization",
            "name": "Fronius International GmbH",
            "sameAs": [
                "https://www.fronius.com",
                "https://www.facebook.com/fronius.int",
                "https://www.youtube.com/user/FroniusInternational",
                "https://www.linkedin.com/company/froniusinternational",
                "https://www.xing.com/pages/froniusinternational"
            ]
        },
        {
            "@type": "Organization",
            "name": "rtrace.io",
            "sameAs": [
                "https://blog.rtrace.io"
            ]
        }
    ],
    "alumniOf": [
        {
            "@type": "CollegeOrUniversity",
            "name": "FH Joanneum",
            "sameAs": "https://www.fh-joanneum.at"
        }
    ],
    "gender": "Male",
    "Description": "Technologist",
    "disambiguatingDescription": "Owner of rtrace.io",
    "jobTitle": "Team Lead",
    "worksFor": [
        {
            "@type": "Organization",
            "name": "Fronius International GmbH",
            "sameAs": [
                "https://www.fronius.com",
                "https://www.facebook.com/fronius.int",
                "https://www.youtube.com/user/FroniusInternational",
                "https://www.linkedin.com/company/froniusinternational",
                "https://www.xing.com/pages/froniusinternational"
            ]
        },
        {
            "@type": "Organization",
            "name": "rtrace.io",
            "sameAs": [
                "https://blog.rtrace.io"
            ]
        }
    ],
    "url": "https://blog.rtrace.io",
    "image": "https://blog.rtrace.io/images/rtraceio-raffael-roundbadge-1024x1024.png",
    "address": {
        "@type": "PostalAddress",
        "addressLocality": "Marchtrenk",
        "addressRegion": "Upper Austria",
        "addressCountry": "Austria"
    },
    "sameAs": [
        "https://blog.rtrace.io/about/me",
        "https://live.rtrace.io",
        "https://soundcloud.com/djraremusic",
        "https://gitlab.com/rehberger.raffael",
        "https://github.com/unclearParadigm",
        "https://codeberg.org/unclearParadigm",
        "https://www.linkedin.com/in/raffael-rehberger-561615132/",
        "https://mastodon.social/@rarepublic",
        "https://www.youtube.com/channel/UCy1fJNXgyNCAKYGbncYsqTQ",
        "https://am.pirateradio.social/@ruffy/",
        "https://www.reverbnation.com/2phazemusic",
        "https://2phaze.bandcamp.com/",
        "https://music.amazon.com/artists/B088F2KQD1/2phaze",
        "https://music.apple.com/us/artist/2phaze/358947385",
        "https://www.deezer.com/de/artist/9596504"
    ]
}
</script>
