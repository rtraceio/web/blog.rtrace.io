---
title: ruffy's PGP public keys
authors: 
    - raffael@rtrace.io
tags:
    - PGP
    - GPG
    - Mail
    - git
    - Signature
    - Signing
    - Encryption
    - E-Mail
    - Commit
    - Gitlab
    - Framagit
    - Github
    - Bitbucket
    - Protonmail
---

Get in touch with me securely by utilzing PGP. Here you can find my PGP public keys for mail and commit signing to verify commits actually originate from me. In case you want to share sensitive information with me, please encrypt your mails with the below mentioned PGP keys. Even if you want to send me some quality Memes use the keys below. Actaully, always use encrypted communication! ¯\\_(ツ)_/¯

<!-- more -->

## git commit signatures

Get my [commit signing GPG public key](https://blog.rtrace.io/misc/publickey.git-d29136197413ff6e92fa19b2040c4b5408a096e7.asc)

```bash
wget https://blog.rtrace.io/misc/publickey.git-d29136197413ff6e92fa19b2040c4b5408a096e7.asc
```

```text
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEYt+gehYJKwYBBAHaRw8BAQdAX/CkluphSPyRY9f7F8XWlkOhlA1YxxSoZ0E2
90nhSNe0JVJhZmZhZWwgUmVoYmVyZ2VyIDxyYWZmYWVsQHJ0cmFjZS5pbz6IlAQT
FgoAPBYhBNKRNhl0E/9ukvoZsgQMS1QIoJbnBQJi36B6AhsDBQsJCAcCAyICAQYV
CgkICwIEFgIDAQIeBwIXgAAKCRAEDEtUCKCW58kqAQDCHIoK5mMP37F3zeVNoTHk
fUcp6AZzWrhqtuRx8/m5yQD/T18jo44vSZrRBw5Kw7W9EjjDLLN4w7lj65AqtcS9
jAe4OARi36B6EgorBgEEAZdVAQUBAQdAdKmIZ0najpGDxzX+nxdEf9VOy1ea3m87
LBDgX43g+3gDAQgHiHgEGBYKACAWIQTSkTYZdBP/bpL6GbIEDEtUCKCW5wUCYt+g
egIbDAAKCRAEDEtUCKCW54XfAP9NRCtEJ+dqH69BXJBQU+3uSXxJYkdWdzQSZzwf
CPbJ+QD9HIYJWW/ezMxFsGeeOggMwW8U6lucKS2TRHYna3SHxQo=
=FHMn
-----END PGP PUBLIC KEY BLOCK-----
```

### Thumbprint

```text
d29136197413ff6e92fa19b2040c4b5408a096e7
```

## encrypted e-mails

Get my [mail PGP public key](https://blog.rtrace.io/misc/publickey.raffael@rtrace.io-dfaa71caa0630b3268fdc9f45e53d807ef71520d.asc) for [raffael@rtrace.io](mailto:raffael@rtrace.io).

```bash
wget https://blog.rtrace.io/misc/publickey.raffael@rtrace.io-dfaa71caa0630b3268fdc9f45e53d807ef71520d.asc
```

```text
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v4.10.10
Comment: https://openpgpjs.org

xjMEXTdyNhYJKwYBBAHaRw8BAQdAtcFzTfmx4RPbZuNGLgmaGDFF+G9c5KGw
Lwsfoc7hSY7NJXJhZmZhZWxAcnRyYWNlLmlvIDxyYWZmYWVsQHJ0cmFjZS5p
bz7CdwQQFgoAHwUCXTdyNgYLCQcIAwIEFQgKAgMWAgECGQECGwMCHgEACgkQ
XlPYB+9xUg1HkwD8Dfp7j/JE/Xp5c0Ko/HeDFnDEfymrNn1R3jzZAnBlHxsA
/RpYcBhNEtmbX1N4W7rVHfvMhb7eksYZqpiXpAXYSlcGzjgEXTdyNhIKKwYB
BAGXVQEFAQEHQM6WmiXMxfpi5a+p3MCvC8DFwkZeqEBdPurrHBhsu6oMAwEI
B8JhBBgWCAAJBQJdN3I2AhsMAAoJEF5T2AfvcVINLL4A+wT/yDrrRke384e7
DShBuxDHKCnAgYRAcWB/K/C5EZA8AP9a34aqOdkcMqpbDCNvAkOV1CEmLeTZ
gNXGjK2+NHVOAg==
=dv9T
-----END PGP PUBLIC KEY BLOCK-----
```

### Thumbprint

```text
dfaa71caa0630b3268fdc9f45e53d807ef71520d
```
