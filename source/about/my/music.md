---
title: ruffy's music
authors: 
    - raffael@rtrace.io
tags:
    - 2Phaze
    - Ruffy Le RaRe
    - Soundcloud
    - Music Production
    - Music
    - Audio
    - FL Studio
    - Dance
    - House
    - Electro
    - Trance
    - Drum & Bass
    - Hardstyle
    - Harddance
    - Dubstep
    - Hardcore
    - Happy Hardcore
    - Hands Up
    - Bootleg
    - Remix
    - DJ
    - Production
    - Mashup
    - Mash-Up
photos:
    - /images/pages/about-sets-logo.jpg
---

I make music and here you can find an excerpt of my work. I release my work as **Ruffy Le RaRe** for music between 80 and 132 BPM. This includes, LoFi, Hip-Hop, Dubstep, Moodstep, House and Electro House. For everything greater than 132 BPM I release my music as **2Phaze** covering genres such as Hands Up, Trance, Hardstyle, Drum & Bass, Happy Hardcore, Hardcore, Frenchcore and Uptempo. You can find my music on most streaming platforms including Spotify, iTunes, Napster, Amazon Music, Reverbnation, Soundcloud, YouTube Music, YouTube, Bandcamp and Funkwhale. Nearly all of my work is also available as free high quality download here on [blog.rtrace.io](/).

<!-- more -->

I make music and here you can find an excerpt of my work. I release my work as **Ruffy Le RaRe** for music between 80 and 132 BPM. This includes, LoFi, Hip-Hop, Dubstep, Moodstep, House and Electro House. For everything greater than 132 BPM I release my music as **2Phaze** covering genres such as Hands Up, Trance, Hardstyle, Drum & Bass, Happy Hardcore, Hardcore, Frenchcore and Uptempo. You can find my music on most streaming platforms including Spotify, iTunes, Napster, Amazon Music, Reverbnation, Soundcloud, YouTube Music, YouTube, Bandcamp and Funkwhale. Nearly all of my work is also available as free high quality download here on [blog.rtrace.io](/).


## Latest productions & releases

<div class="embed-collection">
    {% soundcloud track 2022921144 %}
    {% soundcloud track 1994376303 %}
    {% soundcloud track 1903785353 %}
    {% soundcloud track 1888360815 %}
    {% soundcloud track 1789935916 %}
    {% soundcloud track 1703694750 %}
    {% soundcloud track 1622185356 %}
    {% soundcloud track 1606208268 %}
    {% soundcloud track 1345664914 %}
    {% soundcloud track 1221158164 %}
    {% soundcloud track 1098710812 %}
    {% soundcloud track 1099278472 %}
    {% soundcloud track 1094870422 %}
    {% soundcloud track 816443107 %}
    {% soundcloud track 727907509 %}
</div>

## Latest DJ Sets

Whenever I'm live on [Jnktn.TV](https://jnktn.tv) or [live.rtrace.io](https://live.rtrace.io), chances are that I pressed the 🔴 record button. You'll find the recordings here. All DJ Sets and sessions usually appear here within a week. Stay Tuned!

<div class="embed-collection">
{% youtube ggmuihn8xGg %}
{% youtube w5vUNBAVMv4 %}
{% youtube 7aAWkiGOUT0 %}
{% youtube R3v3n3pRvZo %}
{% youtube XDPbZ3eO410 %}
</div>
