---
title: Security
authors: 
    - raffael@rtrace.io
tags:
    - Security
    - Security Policy
    - Terms
---

Security researchers are welcome and free to toy around on `rtrace.io` and its subdomains with some restrictions. However, as `rtrace.io` is a private project with no commercial interest, I'm afraid I cannot pay huge bug bounty rewards even if you found and responsibly reported a vulnerability. 

<!-- more -->

## Policy

Security researchers are welcome and free to toy around on `rtrace.io` and its subdomains with some restrictions. However, as `rtrace.io` is a private project with no commercial interest, I'm afraid I cannot pay huge bug bounty rewards even if you found and responsibly reported a vulnerability. Nontheless, I'd more than grateful if you share any findings (especially impacting the privacy of users) with me. Depending on the extent of the identified security issue, I'm willing to pay rewards ranging between `10€` and `100€`. I'm hosting `rtrace.io` and all of its services, because I believe the world needs more privacy respecting services. If you find a vulnerability that undermindes the privacy/security of visitors of `rtrace.io`, you'll be receiving a bug-bounty for sure. If you find a typo (or something along the severity of a typo) you'll only be rewarded with a virtual cookie ¯\\_(ツ)_/¯.

### What you should not do

The following types of attacks must not be executed. I'm hosting the service on various vCPUs and deliberately decided not to utilize DoS protection services due to their (drastical) negative impact on privacy. Thus every kind of attack that makes takes a service out-of-operation (especially with brute force) must NOT be performed.

- (distributed) denial of service attacks
- continuous load/stress tests against services
- resource exhaustion attacks
- anything with brute force
- uploading files with excessive size


### Non-exhaustive list allowed attacks

- Inection Attacks
    - SQL Injection
    - XSLT, XPATH, XXE
    - XXE,
    - LDAP Injections
    - GraphQL Injection
    - CSRF Injection
    - CRLF Injection, CSV Injection
    - SSTI (Server-side template injection)
    - SSRF (Server-side request forgery)
- Web Server Vulnerabilities
    - Path/Directory Traversal
    - File Inclusion Attacks
    - Parameter Pollution
    - Request Smuggling
    - Caching attacks
- Social Engineering (but please no stalking)
    - including Phishing (acceptable for 1 month)



### Reporting a vulnerability

Reports are accepted via electronic mail at [security@rtrace.io](security@rtrace.io). Acceptable message formats are plain text, rich text, and HTML. I encourage you to encrypt submissions using our PGP public key when submitting vulnerabilities. You can find my PGP public key [here](/about/my/pgp). 

- include proof-of-concept code demonstrating the exploitation of the vulnerability
- provide a detailed technical description of the steps required to reproduce the vulnerability
- include a description of tools needed to identify or exploit the vulnerability
- optionally, if helpful attach images/videos

In case you'd like to stay anonymous, I accept/encourage anonymous reports. But please keep in mind that in case you qualify for a bug bounty reward, I need payment details.

### Disclosure

I'm committed to resolve your identified vulnerability as soon as timely possible. Feel free to write blog posts, videos or publish the vulnerability after 2 weeks. In any case, do not publish sensible data, or identities of users. Every user of `rtrace.io` MUST have the right for privacy. Don' steal it from them!