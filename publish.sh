#!/bin/sh

echo "Publish"

docker build \
    -t "$CI_REGISTRY/$CI_PROJECT_PATH/static:$CI_COMMIT_TAG" \
    -t "$CI_REGISTRY/$CI_PROJECT_PATH/static:latest" \
    -t "quay.io/rtraceio/blog.rtrace.io:latest" \
    -t "quay.io/rtraceio/blog.rtrace.io:$CI_COMMIT_TAG" \
    .

docker push "$CI_REGISTRY/$CI_PROJECT_PATH/static:latest"
docker push "$CI_REGISTRY/$CI_PROJECT_PATH/static:$CI_COMMIT_TAG"
docker push "quay.io/rtraceio/blog.rtrace.io:latest"
docker push "quay.io/rtraceio/blog.rtrace.io:$CI_COMMIT_TAG"
