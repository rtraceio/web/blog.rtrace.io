#!/bin/sh

# --------------------------------------------------------------------------------
# <Eptixdark Theme Build>
# --------------------------------------------------------------------------------
cd themes/hexo-theme-eptixdark
echo "Installing Theme Dependencies"
npm install gulp -g
npm install

echo "Building Theme (CSS + JS)"
gulp build

echo "Scaling Images"
apk add --no-cache bash
chmod +x ./scale_images.sh && ./scale_images.sh
cd ../../

# --------------------------------------------------------------------------------
# <Blog Build>
# --------------------------------------------------------------------------------
echo "Installing Hexo"
npm install hexo hexo-cli -g

echo "Installing Dependencies"
npm install

echo "Generating Hexo Build-Artifacts"
hexo generate
